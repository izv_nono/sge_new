<?php
/**
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'notebook');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'notebook');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', 'notebook');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'm{xVk&;4kcgHXg iR[j8=-X(4Zd:Jqk^;RiD>qO2qr1E~ctqNX~_R3pvUUKYq:5q');
define('SECURE_AUTH_KEY', 'Xm;b*nmdG^8bC1)y>XA~s#ns=8Se_~XfH<6H#?Iil?1B-t|DhW$HnPRt1ck*HTJd');
define('LOGGED_IN_KEY', 'sD{12wxWx5UTlH<TKjK>59JHk8ttC<mU_G<R+VVt,oYwgE%0T|>-gQ(>Z~{ZGb+j');
define('NONCE_KEY', 'pEGSU@Hd#X?tes^(5l:#*y>b~/6?yaeHFM)wR=aJQWOh0O1WyD?QPCA DZ=ZxKxl');
define('AUTH_SALT', 'Y0]DH5Re>/v93eYN4$]#sdtPB;_!$NU$Y&hp`*Qz/YYiEgOy>a*OPBOfpAY+E;kx');
define('SECURE_AUTH_SALT', 'A&q[.F2X;Woa:m(gU[,w7MPRxYEFo)ZNo;Haya{Q!O$//FVRkVc+]x/@D`W] sAd');
define('LOGGED_IN_SALT', 'E&gvX+u6ovks(1ymk8(|8#v1>M&sXAB6bdT[><g0)GU)`9RH(k@`62r;<R7SRYZ2');
define('NONCE_SALT', 'x$Jr8)7Xv1%d_}I4}hBA*j_O@:Czl(WZC{H{[yr7N~OSJ!74,WmCG{v){3E}&,&5');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wp_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

