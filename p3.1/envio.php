<?php
/**
 * Created by PhpStorm.
 * User: dam
 * Date: 23/9/16
 * Time: 14:15
 */
$asunto = null;
$mensaje = null;
if (!empty($_POST["envio"])) {
    //Se ha enviado el mensaje
    if (!empty($_POST["email"])) {
        //Tengo email
        if (!empty($_POST["asunto"])) {
            //Tengo asunto
            if (!empty($_POST["mensaje"])) {
                //Tengo mensaje
                $mensaje_error = "Correcto";

                //Datos
                $email = $_POST["email"];
                $asunto = $_POST["asunto"];
                $mensaje = $_POST["mensaje"];

                $enviar_mensajes_masivos = true;


            } else {
                $mensaje_error = "Falta el mensaje";
            }
        } else {
            $mensaje_error = "Falta el asunto";
        }
    } else {
        $mensaje_error = "Falta el email";
    }
} else {
    $mensaje_error = "";
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>SPAM</title>
    <style>
        body {
            background: url("elvis.jpeg");
        }

        form {
            margin-top: 20%;
            margin-left: 40%;
            margin-right: 40%;
            background: #FFF;
        }

        <?php
            if ($mensaje_error == "Correcto"){
                echo "
                .mensaje{
                    color: lime;
                }
                ";
            }else{
                echo "
                .mensaje{
                    color: red;
                }
                ";
            }
        ?>
    </style>
</head>
<body>
<h2>El Rey</h2>
<?php
if ($enviar_mensajes_masivos) {

    //Mensajes masivos
    $con = mysqli_connect("localhost", "root", "izv2dam", "elmer")
    or die ("No se ha podido conectar con BB.DD");

    if ($con == true) {
        echo "<p>MySQL funciona correctamente</p>";
    }

    $consulta = mysqli_query($con, "SELECT * FROM maillist");
    $datos = array();
    $datos2 = array();
    while ($row = mysqli_fetch_array($consulta, MYSQLI_ASSOC)) {

        foreach ($row as $key => $value) {
            $datos[$key] = $value;
        }
        $datos2[] = $datos;
        //Destinatario
        $email = $datos["email"];
        mail($email, $asunto, $mensaje);
        $enviado = "<p>Mensaje enviado para: " . $datos["nombre"] . " " . $datos["apellidos"] . "</p>";
        echo $enviado;

    }

    // echo "<pre>" . print_r($datos2, true) . "</pre>";

    mysqli_close($con);
}
?>

<p class="mensaje"><?= $mensaje_error ?></p>
<form action="envio.php" method="post">
    <fieldset>
        <legend>Enviar mensaje</legend>
        <table>

            <tr>
                <td>
                    <label for="email">Email: </label>
                </td>
                <td>
                    <input type="email" name="email" id="email" maxlength="320"
                           value="<?= !empty($email) ? $email : '' ?> ">
                </td>
            </tr>
            <tr>
                <td>
                    <label for="asunto">Asunto: </label>
                </td>
                <td>
                    <input type="text" name="asunto" id="asunto" maxlength="50"
                           value="<?= !empty($asunto) ? $asunto : '' ?>">

                </td>
            </tr>
            <tr>
                <td>
                    <label for="mensaje">Mensaje: </label>
                </td>
                <td>
                    <textarea name="mensaje" id="mensaje" cols="30"
                              rows="10"><?= !empty($mensaje) ? $mensaje : '' ?></textarea>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="submit" value="Enviar" name="envio">
                </td>
            </tr>
        </table>
    </fieldset>


</form>
</body>
</html>
