<?php
get_header();

get_template_part('template-parts/nav', '2');
?>
<title>Te has perdido</title>
<link rel="stylesheet" href="<?= get_template_directory_uri() . "/" ?>style.css">
<style>
    .white {
        color: whitesmoke;
    }
</style>
<div class="container">

    <div class="row">
        <div class="col-lg-12">
            <form role="search" method="get" id="searchform" action="<?= home_url('/'); ?>">
                <div class="input-group">
                    <input type="text" class="form-control" id="s" name="s" placeholder="<?php _e('Search'); ?>" value="<?= get_search_query() ?>"/>
                    <span class="input-group-btn"><button type="submit" class="btn btn-info"><i class="glyphicon glyphicon-search"></i> <?php _e('Search'); ?></button></span>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="text-center white">
                <h1>Error 404</h1>
                <h2>Enhorabuena has encontrado la nada absoluta</h2>
                <img src="<?= get_template_directory_uri() . "/" ?>img/404.png" alt="">
            </div>
        </div>
    </div>
</div>