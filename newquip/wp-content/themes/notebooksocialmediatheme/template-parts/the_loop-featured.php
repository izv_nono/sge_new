<?php
/**
 * Created by PhpStorm.
 * Date: 2/12/16
 * Time: 14:08
 *
 * the_ID
 * the_author
 * the_title
 * the_title_attribute
 * the_permalink
 * the_time
 */

$query = new WP_Query(
    array(
        'nopaging' => false,
        'posts_per_page' => 1,
        'post_type' => array(
//            'coffee_post',
            'post'
        ),
        /*
        'tax_query' => array(
            array(
                'taxonomy' => 'post_format',
                'field' => 'slug',
                'terms' => array(
                    'post-format-aside',
                    'post-format-gallery',
                    'post-format-link',
                    'post-format-image',
                    'post-format-quote',
                    'post-format-audio',
                    'post-format-video'
                ),
                'operator' => 'NOT IN'
            ),
        ),
        */
    )
);

?>
<?php
if ($query->have_posts()) :
    while ($query->have_posts()) :
        $query->the_post();
        ?>
        <div class="box" id="post-<?php the_ID(); ?>" <?php post_class(); ?>
             data-id="<?php the_ID(); ?>">
            <div class="post-header text-center">
                <h2>
                    <?= get_post_format_spam(); ?>
                    <?= get_the_title(); ?>
                </h2>
                <?php if (function_exists('add_theme_support'))
                    the_post_thumbnail('post-thumbnail', ['class' => 'img-responsive center-block', 'style' => 'background-position:center center;']);
                ?>
                <div class="col-lg-12">
                    <div class="col-lg-3">
                        <time datetime="<?= get_the_time("Y-m-j") ?>" class="icon center-block">
                            <em><?php the_time("l"); ?></em>
                            <strong><?php echo strtoupper(get_the_time("F")); ?></strong>
                            <span><?php the_time("j"); ?></span>
                        </time>
                    </div>
                    <div class="col-lg-12">
                        <h2>
                            <small class="author_and_date">
                                <?= get_my_categories() ?><br>
                                <a href="<?= get_author_posts_url(get_the_author_meta('ID'), get_the_author_meta('user_nicename')); ?>">
                                    <?php the_author(); ?>
                                </a>
                                |
                                <a href="<?= get_day_link(get_the_time('Y'), get_the_time('m'), get_the_time('d')); ?>">
                                    <?= strtolower(get_the_time("j \\d\\e F \\d\\e Y")) ?>
                                </a>
                            </small>
                        </h2>
                    </div>

                </div>
                <?php $thumb = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'medium'); ?>
            </div>
            <div class="row text-center the_excerpt">
                <div class="col-lg-8 col-lg-offset-2">
                    <hr>

                    <p class="text-justify"><?= the_excerpt_max_charlength(600); ?></p>

                    <a href="<?= get_permalink() ?>" class="btn btn-default btn-lg">Leer más</a>
                    <hr>
                </div>
            </div>

            <div class="post-footer">
                <?php edit_post_link(); ?>
                <div class="comments">
                    <?php comments_popup_link('Deja un comentario ', '1 Comentario', '% Comentario'); ?>
                </div>
                <div class="tags"><?php the_tags('Tags: ', ', '); ?></div>
            </div>
        </div>
        <?php
    endwhile;
else :

endif;
wp_reset_postdata();
?>

