<?php
/**
 * Created by PhpStorm.
 * User: dam
 * Date: 13/1/17
 * Time: 13:42
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <table class="table">
                <tr>
                    <th>Fecha</th>
                    <th>Titulo</th>
                </tr>
                <?php
                if (have_posts()) {
                    while (have_posts()) {
                        the_post();
                        ?>
                        <tr>
                            <td>
                                <span class="glyphicon glyphicon-calendar"></span>
                                <?= get_the_date("j \\d\\e M. \\d\\e\\l Y ") ?>
                            </td>
                            <td>
                                <?php
                                the_title(sprintf("<a href='%s'>", esc_url(get_the_permalink()), "</a>"))
                                ?>
                            </td>
                        </tr>
                        <?php
                    }
                }
                ?>
            </table>
        </div>
    </div>
</div>