<!-- Navigation -->
<nav class="navbar navbar-default navbar-fixed-top " role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- navbar-brand is hidden on larger screens, but visible when the menu is collapsed -->
            <a class="navbar-brand" href="index">Business Casual</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
<!--                    <a href="--><?//= get_option('home') ?><!--">Home</a>-->
                    <a href="<?= esc_url(home_url()) ?>">Home</a>
                </li>
                <li>
                    <a href="<?= get_page_link(get_page_by_title('about')->ID) ?>">About</a>
                </li>
                <li>
                    <a href="<?= get_page_link(get_page_by_title('blog')->ID) ?>">Blog</a>
                </li>
                <li>
                    <a href="<?= get_page_link(get_page_by_title('contact')->ID) ?>">Contact</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>
