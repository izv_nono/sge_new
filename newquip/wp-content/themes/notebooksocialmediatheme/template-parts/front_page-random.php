<div class="row">
    <div class="col-lg-12 text-center">
        <div class="box">

            <div class="carousel slide">
                <img src="<?= get_template_directory_uri() . "/" ?>img/slide-<?= $rand = rand(1, 3); ?>.jpg" alt="front page" class="img-responsive img-full">
            </div>
            <h2 class="brand-before">
                <small>Welcome to</small>
            </h2>
            <h1 class="brand-name">Business Casual</h1>
            <hr class="tagline-divider">
            <h2>
                <small>By
                    <strong>Start Bootstrap</strong>
                </small>
            </h2>
        </div>
    </div>


</div>