<?php
/**
 * Created by Antonio.
 * Date: 2/12/16
 * Time: 13:58
 *
 * the_ID
 * the_author
 * the_title
 * the_title_attribute
 * the_permalink
 * the_time
 */


?>
<?php
if (have_posts()) :
    while (have_posts()) :
        the_post();
        $post = get_post();
        ?>
        <div class="box box_breadcrumbs">
            <?= get_breadcrumb(); ?>
        </div>

        <div class="box" id="post-<?php the_ID(); ?>" <?php post_class(); ?>
             data-id="<?php the_ID(); ?>">
            <div class="post-header">
                <p class="small">
                    <span class="author">Autor:
                        <a href="<?= get_author_posts_url(get_the_author_meta('ID'), get_the_author_meta('user_nicename')); ?>">
                            <?php the_author(); ?>
                        </a>
                    </span><br>
                    <span class="date">Fecha:
                        <a href="<?= get_day_link(get_the_time('Y'), get_the_time('m'), get_the_time('d')); ?>"><?php the_time('j. M. Y'); ?></a>
                    </span>
                </p>
                <h3>
                    <a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">
                        <?= get_the_title(); ?>
                    </a>
                </h3>
            </div>
            <hr>
            <div class="entry clear">
                <?php if (function_exists('add_theme_support'))
                    the_post_thumbnail('post-thumbnail', ['class' => 'img-responsive']);
                ?>
                <?php the_content(); ?>
            </div>
            <hr>
            <div class="post-footer">
                <div class="categories"><?= __("Categories") ?>: <?php the_category(' &gt; '); ?></div>
                <div class="comments">
                    <?php comments_popup_link('Deja un comentario ', '1 Comentario', '% Comentario'); ?>
                </div>
                <div class="tags"><?php the_tags('Tags: ', ', '); ?></div>
                <?php edit_post_link(); ?>
                <?php wp_link_pages(); ?>
            </div>
        </div>
        <div class="box">
            <ul class="pager">
                <li class="previous"><?php next_post_link('%link'); ?></li>
                <li class="next"><?php previous_post_link('%link'); ?></li>
            </ul>
        </div>
        <div class="box">
            <?php
            $fields = array(
                'author' =>
                    '<p class="comment-form-author"><div class="form-group"><label for="author">' . __('Name', 'domainreference') . '</label> ' .
                    ($req ? '<span class="required">*</span>' : '') .
                    '<input id="author" class="form-control" name="author" type="text" value="' . esc_attr($commenter['comment_author']) .
                    '" ' . $aria_req . ' /></div></p>',

                'email' =>
                    '<p class="comment-form-email"><div class="form-group"><label for="email">' . __('Email', 'domainreference') . '</label> ' .
                    ($req ? '<span class="required">*</span>' : '') .
                    '<input id="email" name="email" class="form-control" type="text" value="' . esc_attr($commenter['comment_author_email']) .
                    '" ' . $aria_req . ' /></div></p>',

                'url' =>
                    '<p class="comment-form-url"><div class="form-group"><label for="url">' . __('Website', 'domainreference') . '</label>' .
                    '<input id="url" name="url" class="form-control" type="text" value="' . esc_attr($commenter['comment_author_url']) .
                    '"   /></p>',
            );
            $comments_args = array(
                'title_reply' => 'Discuss this post ?',
                'fields' => apply_filters('comment_form_default_fields', $fields),
                'comment_field' => '<p class="comment-form-comment"><div class="form-group"><label for="comment">' . _x('Comment', 'noun') .
                    '</label><textarea id="comment" name="comment" class="form-control"  rows="8" aria-required="true">' .
                    '</textarea></div></p>',
                'comment_notes_after' => ' ',
                'class_submit' => 'btn btn-default'
            );
            ?>
            <?php //comment_form($comments_args) ?>

            <?php comments_template() ?>

        </div>
        <?php
    endwhile;
else :
endif;
wp_reset_postdata();
?>
