<?php
/**
 * Created by PhpStorm.
 * Date: 2/12/16
 * Time: 14:08
 *
 * the_ID
 * the_author
 * the_title
 * the_title_attribute
 * the_permalink
 * the_time
 */

$args = array(
    'posts_per_page' => '3',
    'nopaging' => false
);
$query = new WP_Query($args);

?>
<?php if ($query->have_posts()) : ?>
    <?php while ($query->have_posts()) : $query->the_post(); ?>
        <div class="col-lg-4 front-page-link">
            <div class="box " id="post-<?php the_ID(); ?>" <?php post_class(); ?> data-id="<?php the_ID(); ?>">
                <div class="post-header text-center">
                    <h2 class="intro-text text-center">
                        <a class="get_the_permalink_class" href="<?= get_the_permalink() ?>">
                            <?= get_the_title(); ?>
                        </a>
                    </h2>
                    <hr>
                    <?php
                    if (function_exists('add_theme_support')) {
//                        the_post_thumbnail('post-thumbnail', ['class' => 'img-responsive center-block', 'style' => 'background-position:center center;']);
                    }
                    ?>
                    <h5>
                        <small>
                            <a href="<?= get_author_posts_url(get_the_author_meta('ID'), get_the_author_meta('user_nicename')); ?>">
                                <?php the_author(); ?>
                            </a>
                            |
                            <a href="<?= get_day_link( get_the_time('Y'), get_the_time('m'), get_the_time('d')); ?>">
                                <?= strtolower(get_the_time("j \\d\\e F \\d\\e Y")) ?>
                            </a>
                        </small>
                    </h5>
                    <?php
                    $thumb = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'medium');
                    ?>
                </div>
                <div class="entry clear text-center">
                    <p class="text-justify small">
                        <?= get_the_excerpt() ?>
                    </p>
                    <a href="<?= get_permalink() ?>" class="btn btn-default btn-lg">Leer más</a>
                </div>
                <hr>
                <small>
                    <?= get_my_categories() ?>
                </small>
            </div>
        </div>
    <?php endwhile; ?>
<?php else : ?>

<?php endif; ?>
<?php
wp_reset_postdata();