<?php
/**
 * Created by PhpStorm.
 * User: dam
 * Date: 16/1/17
 * Time: 9:54
 */
?>
<h2>Posts</h2>
<table class="table">
    <tr>
        <th>Fecha</th>
        <th>Autor</th>
        <th>Entrada</th>
    </tr>
    <?php
    if (have_posts()) {
        while (have_posts()) {
            the_post();
            ?>
            <tr>
                <td><?= get_the_date() ?></td>
                <td><?= get_the_author() ?></td>
                <td>
                    <a href="<?= get_the_permalink() ?>"><?= get_the_title() ?></a><br>
                    <?= get_the_excerpt() ?>
                </td>
            </tr>
            <?php
        }
    }
    ?>
</table>