<?php
/**
 * Created by PhpStorm.
 * Date: 2/12/16
 * Time: 14:08
 *
 * the_ID
 * the_author
 * the_title
 * the_title_attribute
 * the_permalink
 * the_time
 */
global $wp_query;

?>
<div class="box">
    <?php
    get_template_part("template-parts/template", "search");
    if (have_posts()) {
        ?>

        <h3>
            <?php
            echo $count = $wp_query->found_posts;
            $counts = ($count > 1) ? "s" : "";
            if (is_category()) {
                echo " Post" . $counts . " Encontrado" . $counts . " con la categoria de: " . single_cat_title(" ", false);
            } elseif (is_author()) {
                echo " Post" . $counts . " Encontrado" . $counts . " del autor: <a href='" . get_author_posts_url(get_the_author_meta("ID")) . "'>" . get_the_author() . "</a>";
            } elseif (is_day()) {
                echo " Post" . $counts . " Encontrado" . $counts . " del " . get_the_date('j \\d\\e F \\d\\e\\l Y');
            } elseif (is_month()) {
                echo " Post" . $counts . " Encontrado" . $counts . " de " . get_the_date('F \\d\\e\\l Y');
            } elseif (is_year()) {
                echo " Post" . $counts . " Encontrado" . $counts . " del " . get_the_date('Y');
            } elseif (is_date()) {
                echo " Post" . $counts . " Encontrado" . $counts . " del " . get_the_time('j. M. Y');
            } elseif (is_search()) {
                echo " Post" . $counts . " Encontrado" . $counts . " de la busqueda: " . get_search_query();
            }
            ?>
        </h3>
        <?php
    }
    ?>
</div>

