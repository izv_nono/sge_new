<?php
/*
 * Funciones de usuario
 */

//add_filter('excerpt_more', 'new_excerpt_more');
//function new_excerpt_more($more) {
//    global $post;
//    return '';
//}
add_theme_support('post-formats', array('quote', 'video', 'gallery', 'audio', 'aside', 'link'));
/**
 * the_excerpt_max_charlength($charlength)
 * @param $charlength
 * @return string
 */
function the_excerpt_max_charlength($charlength) {
    $excerpt = get_the_excerpt();
    $charlength++;
    $ret = "";
    if (mb_strlen($excerpt) > $charlength) {
        $subex = mb_substr($excerpt, 0, $charlength - 5);
        $exwords = explode(' ', $subex);
        $excut = -(mb_strlen($exwords[count($exwords) - 1]));
        if ($excut < 0) {
            $ret .= mb_substr($subex, 0, $excut);
        } else {
            $ret .= $subex;
        }
        $ret .= '...';
    } else {
        $ret .= $excerpt;
    }
    return $ret;
}

/*∫
function custom_excerpt_length($length) {
    if (!is_home()) {
        return 20;
    } else {
        return $length;
    }
}

add_filter('excerpt_length', 'custom_excerpt_length', 999);
*/

function generaltheme_widgets_init() {
    register_sidebar(
        array(
            'name' => __('Header Widgets'),
            'id' => 'header',
            'description' => __('Header Widget Area'),
            'before_widget' => '<div class="widget %2$s">',
            'after_widget' => '</div>',
        )
    );
    register_sidebar(
        array(
            'name' => __('Sidebar Widgets'),
            'id' => 'sidebar',
            'description' => __('Sidebar Widget Area'),
            'before_widget' => '<div class="widget %2$s">',
            'after_widget' => '</div>',
        )
    );
    register_sidebar(
        array(
            'name' => __('Footer Widgets'),
            'id' => 'footer',
            'description' => __('Footer Widget Area'),
            'before_widget' => '<div class="widget %2$s">',
            'after_widget' => '</div>',
        )
    );
}

add_action('widgets_init', 'generaltheme_widgets_init');

function get_my_categories($args = array()) {
    global $post;//llama a funcion de el bucle

    $defaults = array('fields' => 'names');
    $array_categories = wp_get_post_categories($post->ID, $defaults);

    if (count($array_categories) >= 2) {
        return $array_categories[0] . " & " . $array_categories[1];
    } else {
        return $array_categories[0];
    }
}

add_theme_support('post-thumbnails');

function custom_get_pages($pages, $r) {
    foreach ($pages as $page) {
        if (!is_admin()) {
            $link = get_page_link($page->ID);

            if ($page->post_title == "home") {
//                echo "<li><a href='" . esc_url(home_url()) . "#home'>" . $page->post_title . "</a></li>";
            } else {
                if ($page->post_title != "archives") {
//                    echo "<li><a href='" . $link . "'>" . $page->post_title . "</a></li>";
                }
            }
        }
    }

    return $pages;
}

add_filter('get_pages', 'custom_get_pages', 10, 2);

function extra_profile_fields($user) {
    ?>
    <h3>Redes Sociales</h3>
    <table class="form-table">
        <tr>
            <th><label for="twitter">Twitter</label></th>
            <td>
                <input type="text" name="twitter" id="twitter" value="<?php echo esc_attr(get_the_author_meta('twitter', $user->ID)) ?>" class="regular-text"/> <br/>
                <span class="description">Por favor, Introduzca su usuario de Twitter</span>
            </td>
        </tr>
        <tr>
            <th><label for="facebook">Facebook</label></th>
            <td>
                <input type="text" name="facebook" id="facebook" value="<?php echo esc_attr(get_the_author_meta('facebook', $user->ID)) ?>" class="regular-text"/> <br/>
                <span class="description">Por favor, Introduzca su url de Facebook</span>
            </td>
        </tr>
        <tr>
            <th><label for="google">Google</label></th>
            <td>
                <input type="text" name="google" id="google" value="<?php echo esc_attr(get_the_author_meta('google', $user->ID)) ?>" class="regular-text"/> <br/>
                <span class="description">Por favor, Introduzca su url de google</span>
            </td>
        </tr>
    </table>
    <?php
}

add_action('show_user_profile', 'extra_profile_fields');
add_action('edit_user_profile', 'extra_profile_fields');
function save_extra_profile_fields($user_id) {
    update_usermeta($user_id, 'facebook', $_POST['facebook']);
    update_usermeta($user_id, 'twitter', $_POST['twitter']);
    update_usermeta($user_id, 'google', $_POST['google']);
}

add_action('personal_options_update', 'save_extra_profile_fields');
add_action('edit_user_profile_update', 'save_extra_profile_fields');

function has_gravatar($email) {
    // Craft a potential url and test its headers
    $hash = md5(strtolower(trim($email)));
    $uri = 'http://www.gravatar.com/avatar/' . $hash . '?d=404';
    $headers = @get_headers($uri);
    if (!preg_match("|200|", $headers[0])) {
        $has_valid_avatar = FALSE;
    } else {
        $has_valid_avatar = TRUE;
    }
    return $has_valid_avatar;
}

function extra_profile_skills($user) {
    ?>
    <h3>Skills del usuario</h3>
    <table class="form-table">
        <?php
        $skills = array("One", "Two", "Three", "Four");
        foreach ($skills as $skill_key => $skill_value) {
            ?>
            <tr>
                <th><label for="skill_<?= $skill_key ?>_name">Nombre de la Skill <?= $skill_value ?></label></th>
                <td>
                    <input type="text" name="skill_<?= $skill_key ?>_name" id="skill_<?= $skill_key ?>_name" value="<?php echo esc_attr(get_the_author_meta("skill_" . $skill_key . "_name", $user->ID)) ?>" class="regular-text"/> <br/>
                    <span class="description">Nombre de su skill <?= $skill_value ?></span>
                </td>
            </tr>
            <tr>
                <th><label for="skill_<?= $skill_key ?>_value">Valor de la Skill <?= $skill_value ?></label></th>
                <td>
                    <input type="number" name="skill_<?= $skill_key ?>_value" id="skill_<?= $skill_key ?>_value" value="<?php echo esc_attr(get_the_author_meta("skill_" . $skill_key . "_value", $user->ID)) ?>" class="regular-text" max="100" min="0"/> <br/>
                    <span class="description">Introduzca el valor de su skill <?= $skill_value ?></span>
                </td>
            </tr>
            <?php
        }
        ?>
    </table>
    <?php
}

add_action('show_user_profile', 'extra_profile_skills');
add_action('edit_user_profile', 'extra_profile_skills');

function save_extra_profile_skills($user_id) {
    $skills = array("One", "Two", "Three", "Four");
    foreach ($skills as $skill_key => $skill_value) {
        update_usermeta($user_id, "skill_" . $skill_key . "_name", $_POST["skill_" . $skill_key . "_name"]);
        update_usermeta($user_id, "skill_" . $skill_key . "_value", $_POST["skill_" . $skill_key . "_value"]);
    }
}

add_action('personal_options_update', 'save_extra_profile_skills');
add_action('edit_user_profile_update', 'save_extra_profile_skills');

function get_user_role($userid) {
    return implode(", ", get_userdata($userid)->roles);
}

function notebook_wp_title() {

}

remove_filter('pre_user_description', 'wp_filter_kses');

function addResponsive($content) {
    if ($content) {
        $content = mb_convert_encoding($content, 'HTML-ENTITIES', "UTF-8");
        $document = new DOMDocument();

        //evita los errores de lectura del documento html
        libxml_use_internal_errors(true);

        //creamos un nuevo documento html con el contenido
        $document->loadHTML(utf8_decode($content));

        $todo = $document->getElementsByTagName('*');
        $img = $document->getElementsByTagName('img');
        $block = $document->getElementsByTagName('blockquote');

        foreach ($img as $tag) {
            $tag->setAttribute('class', 'img-responsive');
        }

        foreach ($block as $bl) {
            $anterior = $bl->previousElementSibling;
            echo $anterior->tagName;
            if ($anterior->tagName == 'img') {
                $bl->setAttribute('class', 'pieImagen');
            }
        }
        $html = $document->saveHTML();
        return $html;
    }
    return null;
}

add_filter('the_content', 'addResponsive');


function get_post_format_spam() {
    $ret = "";
    switch (get_post_format()) {
        case 'quote':
            $ret = '<i class="fa fa-quote-left"></i>';
            break;
        case'video':
            $ret = '<i class="fa fa-video-camera"></i>';
            break;
        case 'gallery':
            $ret = '<i class="fa fa-picture-o"></i>';
            break;
        case'audio':
            $ret = '<i class="fa fa-music" ></i>';
            break;
        case 'aside':
            $ret = '<i class="fa fa-align-left"></i>';
            break;
        case 'link';
            $ret = '<i class="fa fa-link"></i>';
            break;
        default:
            $ret = '<span class="glyphicon glyphicon-align-left"></span>';
            break;
    }
    return $ret;
}

//echo "<pre>" . print_r(get_the_category(),true) . "</pre>";
function get_breadcrumb() {
    $breadcrumb = array();
    $breadcrumb[] = '<ul id="breadcrumbs" class="breadcrumbs">';
    $breadcrumb[] = '<a href="' . home_url() . '" rel="nofollow">Home</a><li>';
    if (is_category() || is_single()) {
        $categories = (get_the_category());
        $separator = ' ';
        //echo "<pre>" . print_r($categories, true) . "</pre>";
        if (!empty($categories)) {
            foreach ($categories as $category) {
                $breadcrumb[] = '<a href="' . esc_url(get_category_link($category->term_id)) . '" alt="' . esc_attr(sprintf(__('View all posts in %s', 'textdomain'), $category->name)) . '">' . esc_html($category->name) . '</a>' . $separator;
            }
        }

        if (is_single()) {
            $breadcrumb[] = get_the_title();
        }
    } elseif (is_page()) {
        $breadcrumb[] = get_the_title();
    } elseif (is_search()) {
        $breadcrumb[] = 'Busqueda: ';
    }
    $ret = implode($breadcrumb, '</li><li>');
    return $ret . '</li></ul>';
}

function add_my_class($content) {
    if ($content) {
        $content = mb_convert_encoding($content, 'HTML-ENTITIES', "UTF-8");
        $document = new DOMDocument();

        //evita los errores de lectura del documento html
        libxml_use_internal_errors(true);

        //creamos un nuevo documento html con el contenido
        $document->loadHTML(utf8_decode($content));

        switch (get_post_format()) {
            case "quote":

                $parrafos = $document->getElementsByTagName('p');

                foreach ($parrafos as $tag) {
                    $tag->setAttribute('class', "my-quote");
                }

                break;

            case "link":

                $link = $document->getElementsByTagName('a');

                foreach ($link as $tag) {
                    $tag->setAttribute('class', "fa fa-link");
                }

                break;
        }


        $html = $document->saveHTML();
        return $html;
    }
    return null;
}

add_filter('the_content', 'add_my_class');

require_once "functions/custom_post_type.php";
require_once "functions/pagination.php";


//require_once "functions/shortcodes.php";

?>