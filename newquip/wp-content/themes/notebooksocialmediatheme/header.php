<!DOCTYPE html>
<html>
<head>
    <?php wp_head(); ?>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Template NonoDev96">
    <meta name="author" content="NonoDev96">
    <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>;" charset="<?php bloginfo('charset'); ?>" />

    <meta charset="<?php bloginfo('charset'); ?>">
    <title>
        <?php
        if (function_exists('is_tag') && is_tag()) {
            single_tag_title('Tag Archive for &quot;');
            echo '&quot; - ';
        } elseif (is_archive()) {
            wp_title('');
            echo ' Archivo - ';
        } elseif (is_search()) {
            echo 'Search for &quot;' . wp_specialchars(!empty($s) ? $s : "") . '&quot; - ';
        } elseif (!(is_404()) && (is_single()) || (is_page())) {

        } elseif (is_404()) {
            echo 'Recurso no encontrado - ';
        }
        if (is_home()) {
            bloginfo('name');
            echo ' - ';
            bloginfo('description');
        } else {
            bloginfo('name');
        }
        if (!empty($paged) and $paged > 1) {
            echo ' - page ' . $paged;
        }
        ?>
    </title>

    <!-- Bootstrap Core CSS -->
    <link href="<?= get_stylesheet_uri() ?>" rel="stylesheet">

    <!-- Custom CSS -->
    <!--<link href="/css/business-casual.css" rel="stylesheet">-->

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800"
          rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic"
          rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="<?= get_template_directory_uri() . "/" ?>js/jquery.js"></script>
    <script src="<?= get_template_directory_uri() . "/" ?>js/bootstrap.min.js"></script>

    <!--    --><?php //wp_head(); ?>
</head>
<body>
