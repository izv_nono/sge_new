<?php
/**
 * Created by PhpStorm.
 * User: dam
 * Date: 10/2/17
 * Time: 12:33
 */
/*
function get_paginate_page_link2($type = 'plain', $endsize = 1, $mid_size = 1) {
    global $wp_query, $wp_rewrite;
    $current = get_query_var('paged') > 1 ? get_query_var('paged') : 1;
    if (!in_array($type, array('plain', 'list', 'array'))) $type = 'plain';
    $endsize = absint($endsize);
    $midsize = absint($mid_size);
    $pagination = array(
        'base' => @add_query_arg('paged', '%#%'),
        'format' => '', 'total' => $wp_query->max_num_pages,
        'current' => $current,
        'show_all' => false,
        'end_size' => $endsize,
        'mid_size' => $mid_size,
        'type' => $type,
        'prev_text' => '&lt;&lt;',
        'next_text' => '&gt;&gt;'
    );
    if ($wp_rewrite->using_permalinks()) {
        $pagination['base'] = user_trailingslashit(trailingslashit(remove_query_arg('s', get_pagenum_link(1))) . 'page/%#%/', 'paged');
    }
    if (!empty($wp_query->query_vars['s'])) {
        $pagination['add_args'] = array('s' => get_query_var('s'));
    }
    return paginate_links($pagination);
}
*/

//Funcion utilizada para habilitar la paginación en nuestros posts
function get_paginate_page_link($type = 'plain', $endsize = 1, $midsize = 1) {
    //Nos permite trabajar con la query de WP y reescribir la URL
    global $wp_query, $wp_rewrite;
    /* Obtenemos el numero actual de página -> en una plantilla tpo index OJO! si queremos obtener el número de página de una página estática -> tipo front-page tenemos que cambiar 'paged' por 'page' También estamos validando el número de la página incluyendo si es inferior a 1 le asignamos el valor 1 */
    $current = get_query_var('paged') > 1 ? get_query_var('paged') : 1;
    //Saneamos los valores de los argumentos de entrada
    // //Tipos de elementos que se devolverán en la paginacion por defecto en WP es plain
    if (!in_array($type, array('plain', 'list', 'array'))) $type = 'plain';
    /* absint es una función de WP que convierte un número a su entero no negativo, hace lo mismo que abs(intval($num)) */
    $endsize = absint($endsize);
    $midsize = absint($midsize);
    //Establecemos los valores de los argumentos de la funcion paginate_links()
    $pagination = array(
        //Metemos en la consulta realizada el parámetro paged indicando que paginaremos nuestra consulta
        'base' => @add_query_arg('paged', '%#%'),
        'format' => '',
        //Numero total de paginas de nuestra consulta
        'total' => $wp_query->max_num_pages,
        //Pagina actual (Numero)
        'current' => $current,
        //Mostrar todos los enlaces de cada num de pagina
        'show_all' => false,
        // //How many numbers on either the start and the end list edges.
        'end_size' => $endsize,
        //How many numbers to either side of current page, but not including current page.
        'mid_size' => $midsize,
        'type' => $type,
        //Texto que se indica como previous text
        'prev_text' => '&lt;&lt;',
        //Texto que se indica como next text
        'next_text' => '&gt;&gt;'
    );
    /* El método using_permalinks() dek objeto wp_rewrite de WP devuelve TRUE si nuestro sitio usa alguna clase de permalinks */
    if ($wp_rewrite->using_permalinks()) {
        /* Si usamos permalinks hay que rehacer la URL donde pasaremos el número de página, quitando el argumento s de la url porque puede estar a partir de la última barra de directorio en la propia url user_trailingslashit -> Si los permalinks están configurados para acavar en /, le añade la barra a la url que genere */
        $pagination['base'] = user_trailingslashit(trailingslashit(remove_query_arg('s', get_pagenum_link(1))) . 'page/%#%/', 'paged');
    } /* Si estamos en el template search o archive tenemos que tener en cuenta la variable s que es la que tiene el valor de búsqueda */
    if (!empty($wp_query->query_vars['s'])) {
        $pagination['add_args'] = array('s' => get_query_var('s'));
    }
    return paginate_links($pagination);
}

function custom_pagination() {
    global $wp_query;
    $big = 999999999; // need an unlikely integer
    $pages = paginate_links( array(
        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
        'format' => '?paged=%#%',
        'current' => max( 1, get_query_var('paged') ),
        'total' => $wp_query->max_num_pages,
        'prev_next' => false,
        'type'  => 'array',
        'prev_text'    => __('«'),
        'next_text'    => __('»'),
    ) );
    if( is_array( $pages ) ) {
        $paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
//        echo '<ul class="pagination">';
//        foreach ( $pages as $page ) {
//            echo "<li>$page</li>";
//        }
//        echo '</ul>';
    }
    return $pages;
}

?>