<?php
/**
 * Created by PhpStorm.
 * User: dam
 * Date: 3/2/17
 * Time: 12:31
 */

function reg_post_type_coffee() {
    $supports = array(
        'title',
        'editor',
        'author',
        'thumbnail',
        'excerpt',
        'comments',
        'custom-fields',
        'comments',
        'revisions',
        'post-formats',
    );

    $labels = array(
        'name' => _x('Coffees', 'plural'),
        'singular_name' => _x('Coffee', 'singular'),
        'menu_name' => _x('Coffees', 'admin menu'),
        'name_admin_bar' => _x('Coffee', 'admin bar'),
        'add_new' => _x('Add New', 'add new coffee'),
        //
        'add_new_item' => __('Add New Coffee'),
        'new_item' => __('New Coffee'),
        'edit_item' => __('Edit Coffee'),
        'view_item' => __('View Coffee'),
        'all_items' => __('All Coffees'),
        'search_items' => __('Search Coffees'),
        'parent_item_colon' => __('Parent Coffees:'),
        'not_found' => __('No Coffees found.'),
        'not_found_in_trash' => __('No Coffees found in Trash.')
    );

    $args = array(
        'labels' => $labels,
        'supports' => $supports,
        'public' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'coffee'),
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => 5,

    );

    register_post_type('coffee_post', $args);
}

add_action('init', 'reg_post_type_coffee');

function coffee_meta_box_callback($post) {
    wp_nonce_field("coffee_save_meta_box_data", "coffee_meta_box_nonce");
    $value1 = get_post_meta($post->id, "coffee_name", true);
    $value2 = get_post_meta($post->id, "coffee_price", true);
    $value3 = get_post_meta($post->id, "coffee_from", true);
    $value4 = get_post_meta($post->id, "coffee_type", true);
    ?>
    <label for="coffee_name"><?php _e("Name", "coffee_textdomain"); ?></label>
    <input type="text" id="coffee_name" name="coffee_name" value="<?= esc_attr($value1) ?>">

    <label for="coffee_price"><?php _e("Price", "coffee_textdomain"); ?></label>
    <input type="text" id="coffee_price" name="coffee_price" value="<?= esc_attr($value2) ?>">

    <label for="coffee_from"><?php _e("From", "coffee_textdomain"); ?></label>
    <input type="text" id="coffee_from" name="coffee_from" value="<?= esc_attr($value3) ?>">

    <label for="coffee_type"><?php _e("Type", "coffee_textdomain"); ?></label>
    <input type="text" id="coffee_type" name="coffee_type" value="<?= esc_attr($value4) ?>">
    <?php
}

function coffee_add_meta_box() {
    $screens = array('coffee_post');
    foreach ($screens as $screen) {
        add_meta_box(
            "coffee_sectionid",
            __("Coffee details", "coffee_textdomain"),
            "coffee_meta_box_callback",
            $screen,
            "normal"
        );
    }
}

add_action("add_meta_boxes", "coffee_add_meta_box");