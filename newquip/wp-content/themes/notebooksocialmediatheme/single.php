<?php
/*
 * Template Name: blog
 */
get_header();

get_template_part('template-parts/nav', '2');
//require_once "nav.php";
?>
<div class="container">
    <div class="row">
        <aside>
            <div class="col-lg-8">
                <?php
                get_template_part('template-parts/the_loop','single');
                ?>
            </div>
        </aside>
        <aside>
            <div class="col-lg-4">
                <div class="box">
                    <?php get_sidebar() ?>
                </div>
            </div>
        </aside>
    </div>
</div>
<!-- /.container -->
<?php get_footer(); ?>
