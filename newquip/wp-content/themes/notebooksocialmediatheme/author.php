<?php
/**
 * Created by PhpStorm.
 * User: dam
 * Date: 16/1/17
 * Time: 9:59
 */
get_header();

get_template_part('template-parts/nav', '2');
$curauth = (get_query_var('author_name')) ? get_user_by('slug', get_query_var('author_name')) : get_userdata(get_query_var('author'));
?>
<div class="container">
    <div class="row">
        <div class="col-lg-8">
            <div class="box">
                <?php get_template_part("template-parts/template", "search"); ?>
            </div>
            <div class="box">
                <?php get_template_part("template-parts/content", "author"); ?>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="box">

                <?php
                //imagen de usuario
                if (has_gravatar($curauth->user_email)) {
                    $img_gravatar = "https://s.gravatar.com/avatar/" . md5($curauth->user_email);
                } else {
                    $img_gravatar = get_template_directory_uri() . "/img/intro-pic.jpg";
                }
                ?>
                <p><img class="img-responsive center-block" src="<?= $img_gravatar ?>" alt="img usuario"></p>
                <p><strong><?php _e('Name'); ?>: </strong><?= $curauth->first_name ?></p>
                <p><strong><?php _e('LastName'); ?>: </strong><?= $curauth->last_name ?></p>
                <p><strong>Mail: </strong><a href="mailto:<?= $curauth->user_email ?>?Subject=Hello%20again" target="_top"><?= $curauth->user_email ?></a></p>
                <!--
                <?php if (!empty($twitter = get_the_author_meta("twitter", $curauth->ID))) { ?>
                    <p><strong>Twitter: </strong><?= $twitter ?></p>
                <?php } ?>
                <?php if (!empty($facebook = get_the_author_meta("facebook", $curauth->ID))) { ?>
                    <p><strong>Facebook: </strong><?= $facebook ?></p>
                <?php } ?>
                -->
                <blockquote class="small">
                    <p><strong>Descripción: </strong></p>
                    <?= $curauth->description ?>
                </blockquote>
                <p><strong>Rol: </strong><?= get_user_role($curauth->ID) ?></p>
                <?php
                $progress_bar = array("info", "success", "warning", "danger");
                ?>
                <?php for ($i = 0; $i <= 3; $i++) { ?>
                    <p class="small"><strong>Skill: </strong><?= get_the_author_meta("skill_" . $i . "_name", $curauth->ID) ?></p>
                    <div class="progress progress-striped active" style="height: 10px">
                        <div class="progress-bar progress-bar-<?= $progress_bar[$i] ?>" style="width: <?= get_the_author_meta("skill_" . $i . "_value", $curauth->ID) ?>%; "></div>
                    </div>
                <?php } ?>

                <h2><?php _e('Search'); ?></h2>
                <?php
                //Buscador
                get_template_part("template-parts/template", "search");
                ?>

                <h2 class="widget-title"><?php _e('Author'); ?></h2>
                <ul>
                    <?php
                    wp_list_authors(
                        array(
                            'show_fullname' => true,
                            'optioncount' => true,
                            'orderby' => 'post_count',
                            'order' => 'DESC',
                            'number' => 3,
                            'exclude' => array($curauth->ID),
                        )
                    );
                    ?>
                </ul>
                <h2 class="widget-title">Seguir navegando</h2>
                <ul>
                    <li><a href="archives.php">Archives</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
