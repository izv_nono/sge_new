<?php
/*
 * Template Name: archives
 */

get_header();
get_template_part('template-parts/nav', '2');
?>

<div class="container">
    <div class="row">
        <div class="col-lg-8">
            <div class="box">
                <?php echo get_page_template_slug( $post->ID ); ?>



            </div>
        </div>
        <div class="col-lg-4">
            <div class="box">
                <?php get_sidebar() ?>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
