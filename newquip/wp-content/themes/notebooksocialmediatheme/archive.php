<?php
/*
 * Template Name: archive
 */

get_header();
get_template_part('template-parts/nav', '2');
?>
<div class="container">

    <div class="row">
        <div class="col-lg-12 text-center">
            <?php get_template_part('template-parts/posts_found'); ?>
        </div>
    </div>

</div>

<div class="container">
    <div class="row">
        <aside>
            <div class="col-lg-8">
                <?php get_template_part('template-parts/the_loop', 'archive'); ?>
            </div>
        </aside>
        <aside>
            <div class="col-lg-4">
                <div class="box">
                    <?php get_sidebar() ?>
                </div>
            </div>
        </aside>
    </div>
</div>
<?php get_footer(); ?>
