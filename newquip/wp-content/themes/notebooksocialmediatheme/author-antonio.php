<?php
/**
 * Created by PhpStorm.
 * User: dam
 * Date: 16/1/17
 * Time: 9:52
 */

get_header();

get_template_part('template-parts/nav', '2');

$curauth = (get_query_var('author_name')) ? get_user_by('slug', get_query_var('author_name')) : get_userdata(get_query_var('author'));
?>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="box">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-4 text-center">
                                <?php
                                if (has_gravatar($curauth->user_email)) {
                                    $img_gravatar = "https://s.gravatar.com/avatar/" . md5($curauth->user_email);
                                } else {
                                    $img_gravatar = get_template_directory_uri() . "/img/intro-pic.jpg";
                                }
                                ?>
                                <div class="">
                                    <a href="<?php echo get_author_posts_url(get_the_author_meta('ID'), get_the_author_meta('user_nicename')); ?>">
                                        <img src="<?= $img_gravatar ?>" alt="img usuario" class="img-responsive center-block" style="width: 60%">
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <p><strong>Nombre: </strong><?= $curauth->first_name ?></p>
                                <p><strong>Apellidos: </strong><?= $curauth->last_name ?></p>
                                <p><strong>Mail: </strong><a href="mailto:izv.dam.nono@gmail.com?Subject=Hello%20again"><?= $curauth->user_email ?></a></p>
                                <p>
                                    <a href="http://twitter.com/<?php the_author_meta("twitter", $curauth->ID) ?>" class="icon-button twitter"><i class="fa fa-twitter"></i><span></span></a>
                                    <a href="http://facebook.com/<?php the_author_meta("facebook", $curauth->ID) ?>" class="icon-button facebook"><i class="fa fa-facebook"></i><span></span></a>
                                    <a href="http://plus.google.com/<?php the_author_meta("google", $curauth->ID) ?>" class="icon-button google-plus"><i class="fa fa-google-plus"></i><span></span></a>
                                </p>
                            </div>

                            <div class="col-lg-4">
                                <p><strong>Rol: </strong><?= get_user_role($curauth->ID); ?></p>
                                <div class="small">
                                    <?= $curauth->description ?>
                                </div>
                                <!--
                            <?php for ($i = 0; $i <= 3; $i++) { ?>
                                <p class="small"><strong>Skill: </strong><?= get_the_author_meta("skill_" . $i . "_name", $curauth->ID) ?><br>
                                    <strong>Valor: </strong><?= get_the_author_meta("skill_" . $i . "_value", $curauth->ID) ?></p>
                            <?php } ?>
                            -->
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-lg-12 center-block">
                                <script src="<?= get_template_directory_uri() . "/" ?>js/knob.js"></script>
                                <?php
                                for ($i = 0; $i < 4; $i++) {
                                    $skill_name = get_the_author_meta("skill_" . $i . "_name", $curauth->ID);
                                    $skill_value = get_the_author_meta("skill_" . $i . "_value", $curauth->ID);
                                    ?>
                                    <div class="col-lg-3 text-center">
                                        <label>
                                            <span><?= $skill_name ?></span><br>
                                            <input type="text" value="<?= $skill_value ?>" class="dial">
                                        </label>
                                    </div>
                                    <?php
                                }
                                ?>
                                <script>
                                    $(function () {
                                        $(".dial").knob({
                                            "readOnly": true,
                                            "angleArc": 180,
                                            "fgColor": "#8b9dc3",
                                            "bgColor": "#Dfe3ee",
                                            "rotation": "anticlockwise",
                                            "angleOffset": 270,
                                            "width": "60%"
                                        });
                                    });
                                </script>
                            </div>
                        </div>
                    </div>
                    <?php get_template_part("template-parts/content", "listauthor"); ?>
                </div>
                <div class="box">
                    <div class="row">
                        <div class="col-lg-4">
                            <h2 class="widget-title"><?php _e('Search'); ?></h2>
                            <?php get_template_part("template-parts/template", "search"); ?>
                        </div>
                        <div class="col-lg-3 col-lg-offset-1">
                            <h2 class="widget-title"><?php _e('Author'); ?></h2>
                            <ul>
                                <?php
                                wp_list_authors(
                                    array(
                                        'show_fullname' => true,
                                        'optioncount' => true,
                                        'orderby' => 'post_count',
                                        'order' => 'DESC',
                                        'number' => 3,
                                        'exclude' => array($curauth->ID),
                                    )
                                );
                                ?>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-lg-offset-1">
                            <h2 class="widget-title"><?php _e('Pages'); ?></h2>
                            <ul>
                                <?php wp_page_menu() ?>
                            </ul>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>