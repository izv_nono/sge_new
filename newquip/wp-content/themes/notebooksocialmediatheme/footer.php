<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <p>Copyright &copy; Your Website <?= date("Y") ?></p>
            </div>
        </div>
    </div>
</footer>

<!-- jQuery -->


<!-- Script to Activate the Carousel -->
<script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
</script>
<?php
wp_footer();
?>
</body>

</html>