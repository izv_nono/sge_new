<?php
/*
 * Template Name: blog
 */
get_header();

get_template_part('template-parts/nav', '2');
//require_once "nav.php";
?>
<div class="container">
    <div class="row">
        <div class="col-lg-12 text-center">

            <?php get_template_part('template-parts/the_loop', 'featured'); ?>

        </div>
    </div>

    <div class="row">
        <div class="col-lg-8">

            <?php get_template_part('template-parts/the_loop'); ?>

            <div class="box text-center center-block">


                <ul class="pagination">
                    <li class="disabled"><a href="#">&laquo;</a></li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">&raquo;</a></li>
                </ul>
                <?php // echo(get_paginate_page_link('array')); ?>
                <pre>
                    <?php print_r(custom_pagination()); ?>
                </pre>
                <ul class="pagination">
                    <?php
                    foreach (custom_pagination() as $page) {
                        ?>
                        <li><?= $page ?></li>
                        <?php
                    }
                    ?>
                </ul>
            </div>

        </div>
        <div class="col-lg-4">
            <div class="box">

                <?php get_sidebar() ?>

            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
