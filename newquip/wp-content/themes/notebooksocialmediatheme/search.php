<?php
/*
 * Template Name: blog
 */
get_header();

get_template_part('template-parts/nav', '2');

?>
<div class="container">
    <div class="row">
        <div class="col-lg-12 text-center">

            <?php get_template_part('template-parts/posts_found'); ?>

        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <aside>
            <div class="col-lg-12">

                <?php
                if (have_posts()):
                    get_template_part('template-parts/content', 'list');
                else:
                    get_template_part('template-parts/content', 'none');
                endif;
                ?>

            </div>
        </aside>
    </div>
</div>
<?php get_footer(); ?>
