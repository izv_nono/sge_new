-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:8889
-- Tiempo de generación: 28-09-2016 a las 13:23:39
-- Versión del servidor: 5.6.28
-- Versión de PHP: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de datos: `guitarwars`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `score_list`
--

CREATE TABLE `score_list` (
  `id` int(6) NOT NULL,
  `score` int(2) NOT NULL,
  `name` varchar(20) NOT NULL,
  `date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `score_list`
--
ALTER TABLE `score_list`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `score_list`
--
ALTER TABLE `score_list`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;

ALTER TABLE `score_list` ADD `screenshot`
  VARCHAR(128) NOT NULL AFTER `date`;
