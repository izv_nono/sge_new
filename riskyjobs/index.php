<?php
/**
 * User: dam
 * Date: 2/11/16
 * Time: 13:58
 */
require_once "_MySql.php";
require_once "_Nav.php";

function clear($string) {
    $string2 = str_replace(array('.', ','), ' ', $string);
    return preg_replace('/[^A-Za-z0-9\-:]/', ' ', $string2);
}

function data_get_post() {
    $ret = array();
    $buscar = !empty($_REQUEST['query']) ? clear(trim($_REQUEST['query'])) : "";
    $ret['query'] = substr($buscar, 0, 25);
    $ret['page'] = (!empty($_REQUEST['page']) and $_REQUEST['page'] >= 0) ? $_REQUEST['page'] : 0;
    $order = !empty($_REQUEST['order']) ? $_REQUEST['order'] : 0;
    switch ($order) {
        default:
        case 1:
            $ret['order'] = 'title';
            $ret['order_c'] = '1';
            break;
        case 2:
            $ret['order'] = 'description';
            $ret['order_c'] = '2';
            break;
        case 3:
            $ret['order'] = 'city';
            $ret['order_c'] = '3';
            break;
        case 4:
            $ret['order'] = 'state';
            $ret['order_c'] = '4';
            break;
        case 5:
            $ret['order'] = 'zip';
            $ret['order_c'] = '5';
            break;
        case 6:
            $ret['order'] = 'company';
            $ret['order_c'] = '6';
            break;
        case 7:
            $ret['order'] = 'date_posted';
            $ret['order_c'] = '7';
            break;
    }

    return $ret;
}


function string_to_array($string) {
    $array_clear = array();
    $array_clear_temp = explode(' ', $string);
    foreach ($array_clear_temp as $key => $value) {
        if (!empty($value))
            $array_clear[] = $value;
    }

    return $array_clear;
}

function search($string) {
    return string_to_array(clear($string));
}


function highlights($array_riskyjobs, $buscar_implode) {
    $new_array_riskyjobs = array();
    foreach ($array_riskyjobs as $key_array_riskyjob => $value_array_riskyjob) {
        $new_array_riskyjobs[$key_array_riskyjob] = highlight($value_array_riskyjob, $buscar_implode);
    }

    return $new_array_riskyjobs;
}

function highlight($text, $words) {
    if (strpos($text, $words) >= 50) {
        $pos = strpos($text, $words);
        $text = substr($text, 0, $pos + strlen($words));
        $text .= "...";
    } else {
//        $text = substr($text, 0, 50 + strlen($words));
//        $text .= "...";
    }

    preg_match_all('/\w+/', $words, $m);
    if (!$m)
        return $text;
    $re = '/(' . implode('|', ($m[0])) . ')/i';
    $ret = preg_replace($re, "<mark>$0</mark>", $text);
    return $ret;
}

$array_riskyjobs = array();

$data = data_get_post();
if ($data['query'] != null) {

    $buscar = search($data['query']);
    $sql = "
    SELECT *
    FROM riskyjobs ";

    foreach ($buscar as $key => $value) {
        if ($key == 0) {
            $sql .= "WHERE description LIKE '%" . $value . "%' ";
        } else {
            $sql .= "AND description LIKE '%" . $value . "%' ";
        }
        $sql .= "OR title LIKE '%" . $value . "%' ";
        $sql .= "OR city LIKE '%" . $value . "%' ";
        $sql .= "OR state LIKE '%" . $value . "%' ";
        $sql .= "OR zip LIKE '%" . $value . "%' ";
        $sql .= "OR company LIKE '%" . $value . "%' ";
        $sql .= "OR date_posted LIKE '%" . $value . "%' ";
    }

    $sql .= "ORDER BY " . $data['order'] . " ASC ";
//    $sql .= "LIMIT " . $data['page'] * 5 . ", 5 ";

    $data_mysqli = mysqli_query($con, $sql);
    while ($row = mysqli_fetch_array($data_mysqli, MYSQLI_ASSOC)) {
        $array_riskyjobs[] = $row;
    }
}

?>
<header>
    <nav>
        <!--        <h2>Risky Jobs</h2>-->
    </nav>
</header>

<section>
    <article>
        <div class="buscador">
            <form action="index.php" method="post" enctype="multipart/form-data">
                <fieldset>
                    <legend align="center">Risky Jobs</legend>
                    <label for="query_id"></label>
                    <input type="text" name="query" value="<?= $data['query'] ?>" id="query_id" maxlength="25">

                    <input type="hidden" name="page" value="0<?//= $data['page'] ?>">
                    <input type="hidden" name="order" value="<?= $data['order_c'] ?>">

                    <input type="submit" value="Buscar">
                </fieldset>
            </form>
        </div>
    </article>

    <article>
        <?php
        if (!empty($array_riskyjobs)) {
            ?>
            <table>
                <tr>
                    <?php
                    $order_data = array();
                    $order_data['1'] = 'Title';
                    $order_data['2'] = 'Description';
                    $order_data['3'] = 'City';
                    $order_data['4'] = 'State';
                    $order_data['5'] = 'Zip';
                    $order_data['6'] = 'Company';
                    $order_data['7'] = 'Date posted';
                    ?>
                    <?php
                    foreach ($order_data as $key_order => $value_order) {
                        ?>
                        <th>
                            <form action="index.php" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="query" value="<?= $data['query'] ?>">
                                <input type="hidden" name="page" value="<?= $data['page'] ?>">
                                <input type="hidden" name="order" value="<?= $key_order ?>">

                                <input type="submit" value="<?= $value_order ?>">
                            </form>
                        </th>
                        <?php
                    }
                    ?>
                </tr>

                <?php

                foreach (array_slice($array_riskyjobs, $data['page'] * 5, 5) as $jobs_key => $jobs_value) {
                    $buscar = $data['query'];
                    if (is_array($data['query'])) {
                        $buscar = implode(' ', $data['query']);
                    }
                    $jobs_value = highlights($jobs_value, $buscar);

                    echo "
                    <tr>
                        <td>" . ($jobs_value['title']) . "</td>
                        <td class='description'>" . ($jobs_value['description']) . "</td>
                        <td>" . ($jobs_value['city']) . "</td>
                        <td>" . ($jobs_value['state']) . "</td>
                        <td>" . ($jobs_value['zip']) . "</td>
                        <td>" . ($jobs_value['company']) . "</td>
                        <td>" . ($jobs_value['date_posted']) . "</td>
                    </tr>
                    ";

                }

                ?>
            </table>
            <?php
        }
        ?>
    </article>
</section>

<footer>
    <div class="center">
        <ul class="pagination">
            <?php

            if ($data['page'] > 0) {
                ?>
                <li>
                    <form action="index.php" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="query" value="<?= $data['query'] ?>">
                        <input type="hidden" name="page" value="<?= $data['page'] - 1 ?>">
                        <input type="hidden" name="order" value="<?= $data['order'] ?>">

                        <input type="submit" value="«">
                    </form>
                </li>
                <?php
            }
            ?>
            <?php
            for ($i = 0; $i < sizeof($array_riskyjobs) / 5; $i++) {
                $active = '';
                if ($i == $data['page']) {
                    $active = 'class="active"';
                }
                ?>
                <li>
                    <form action="index.php" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="query" value="<?= $data['query'] ?>">
                        <input type="hidden" name="page" value="<?= $i ?>">
                        <input type="hidden" name="order" value="<?= $data['order'] ?>">

                        <input type="submit" value="<?= $i ?>" <?= $active ?>>
                    </form>
                </li>
                <?php
            }
//            echo (int)(sizeof($array_riskyjobs) / 5) . "+" . $data['page'];
            if ((int)(sizeof($array_riskyjobs) / 5) > $data['page']) {
                ?>
                <li>
                    <form action="index.php" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="query" value="<?= $data['query'] ?>">
                        <input type="hidden" name="page" value="<?= $data['page'] + 1 ?>">
                        <input type="hidden" name="order" value="<?= $data['order'] ?>">

                        <input type="submit" value="»">
                    </form>
                </li>
                <?php
            }
            ?>
        </ul>
    </div>
    <p class="p_footer">Desarrollado por <b>IZV - DAM © </b></p>

</footer>