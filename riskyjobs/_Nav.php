<?php
/**
 * Created by PhpStorm.
 * User: dam
 * Date: 2/11/16
 * Time: 13:53
 */

?>
<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Risky Jobs</title>
    <link rel="stylesheet" href="/assets/css/riskyjobs.css">
</head>
<body>
