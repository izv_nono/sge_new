<?php
/**
 * Created by PhpStorm.
 * User: Unchicodelavida
 * Date: 11/11/2016
 * Time: 15:28
 */
function highlight($text, $words) {
    preg_match_all('~\w+~', $words, $m);
    if (!$m)
        return $text;
    $re = '~\\b(' . implode('|', $m[0]) . ')\\b~';
    return preg_replace($re, '<mark>$0</mark>', $text);
}

$text = '
Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
consequat. mark
';

$words = 'ipsum mark labore ';

echo highlight($text, $words);
