<?php
/**
 * Created by PhpStorm.
 * User: nono
 * Date: 3/10/16
 * Time: 9:48
 */

require_once("authentication.php");
require_once("mysql.php");
?>
<?php
require_once("header.php");

/*
 * Codigo insertar
 */

?>
    <form action="mostrar.php" method="post" enctype="multipart/form-data">
        <fieldset>
            <legend>Tu puntuacion en Guitar Wars!!</legend>
            <table border="1">
                <tr>
                    <td><label for="name_id">Nombre</label></td>
                    <td><input type="text" id="name_id" name="name"></td>
                </tr>
                <tr>
                    <td><label for="score_id">Puntuación</label></td>
                    <td><input type="number" id="score_id" name="score"></td>
                </tr>
                <tr>
                    <td><label for="file_id">Archivo</label></td>
                    <td><input type="file" id="file_id" name="screenshot"></td>
                </tr>

                <tr>
                    <td><input type="submit" value="Enviar" name="submit"></td>
                </tr>

            </table>
        </fieldset>
    </form>
<?php

require_once("footer.php");