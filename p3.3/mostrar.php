<?php
/**
 * Created by PhpStorm.
 * User: nono
 * Date: 3/10/16
 * Time: 9:41
 */

require_once("authentication.php");
require_once("mysql.php");
?>
<?php
require_once("header.php");

/*
 * Codigo mostrar
 */


if (!empty($_POST["submit"])) {
    if (!empty($_POST["name"])) {
        if (!empty($_POST["score"])) {
            if (!empty($_FILES["screenshot"])) {
                $screenshot = time() . $_FILES["screenshot"]["name"];
                $ruta_temporal = $_FILES["screenshot"]["tmp_name"];

                move_uploaded_file($ruta_temporal, RUTA_IMG . $screenshot);
            }
            if ($_FILES["screenshot"]["name"] == null) {
                $screenshot = "no_pic.png";
            }


            $name = $_POST["name"];
            $score = $_POST["score"];

            $sql = "
                INSERT INTO 
                score_list (id, score, name, date, screenshot) 
                    VALUES (NULL , " . $score . ", '" . $name . "', now(), '" . $screenshot . "');";
            //echo $sql;
            if (mysqli_query($con, $sql)) {
                echo "<h3>Insertado</h3>";
            } else {
                echo "<h3>Error</h3>";
            }
        }
    }
}


$consulta = mysqli_query($con, "SELECT * FROM score_list WHERE valido = 1");
$datos = array();
$datos2 = array();

$i = 0;
while ($row = mysqli_fetch_array($consulta, MYSQLI_ASSOC)) {
    foreach ($row as $key => $value) {
        $datos[$key] = $value;
    }
    $datos2[$i] = $datos;

    $i++;
}
?>
    <table border="1">
        <thead>
        <tr>
            <th>ID</th>
            <th>Score</th>
            <th>Name</th>
            <th>Date</th>
            <th>Valido</th>
            <th>Screenshot</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($datos2 as $rows_id => $row) {
            ?>

            <tr>
                <?php
                foreach ($row as $key => $value) {
                    ?>
                    <td>
                        <?php
                        if ($key != "screenshot") {
                            echo $value;
                        } else {
                            echo "<img src='../img/" . $value . "' alt='' height='100px'>";
                        }
                        ?>
                    </td>
                    <?php
                }
                ?>

            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
<?php


//echo "<pre>" . print_r($datos2, true) . "</pre>";
mysqli_close($con);


require_once("footer.php");
?>