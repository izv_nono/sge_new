<?php
/**
 * Created by PhpStorm.
 * User: nono
 * Date: 3/10/16
 * Time: 9:46
 */
require_once("authentication.php");
require_once("mysql.php");
?>
<?php
require_once("header.php");

/*
 * Codigo admin
 */

$consulta = mysqli_query($con, "SELECT * FROM score_list");
$datos = array();
$datos2 = array();

$i = 0;
while ($row = mysqli_fetch_array($consulta, MYSQLI_ASSOC)) {
    foreach ($row as $key => $value) {
        $datos[$key] = $value;
    }
    $datos2[$i] = $datos;

    $i++;
}
?>
    <table border="1">
        <thead>
        <tr>
            <th>ID</th>
            <th>Score</th>
            <th>Name</th>
            <th>Date</th>
            <th>Screenshot</th>
            <th>Válido</th>
            <th>Aprobar</th>
            <th>Borrar</th>
        </tr>
        </thead>

        <tbody>
        <?php
        foreach ($datos2 as $rows_id => $row) {
            ?>

            <tr>
                <?php
                foreach ($row as $key => $value) {
                    ?>
                    <td>
                        <?php
                        if ($key != "screenshot") {
                            echo $value;
                        } else {
                            echo "<img src='../img/" . $value . "' alt='' height='100px'>";
                        }
                        ?>
                    </td>
                    <?php
                }
                ?>
                <td>
                    <form action="borrar.php" method="GET">
                        <input type="hidden" name="id" value="<?= $row["id"] ?>">
                        <input type="hidden" name="img" value="<?= $row["screenshot"] ?>">

                        <input type="submit" value="Borrar">
                    </form>
                </td>

            </tr>
            <tr>
                <td>
                    <form action="aprobar.php"></form>
                </td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
<?php
require_once("footer.php");
?>