-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 26-09-2016 a las 14:15:07
-- Versión del servidor: 5.6.28
-- Versión de PHP: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de datos: `elmer`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `maillist`
--

CREATE TABLE `maillist` (
  `id` int(11) NOT NULL,
  `nombre` varchar(20) DEFAULT NULL,
  `apellidos` varchar(40) DEFAULT NULL,
  `telefono` int(9) DEFAULT NULL,
  `ciudad` varchar(20) DEFAULT NULL,
  `calle` varchar(20) DEFAULT NULL,
  `numero` varchar(20) DEFAULT NULL,
  `email` varchar(320) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `maillist`
--

INSERT INTO `maillist` (`id`, `nombre`, `apellidos`, `telefono`, `ciudad`, `calle`, `numero`, `email`) VALUES
(1, 'Antonio', 'Machuca', 651646088, 'Granada', 'C/ Palencia', '3', 'nono.frailes@gmail.com'),
(3, 'Antonio', 'Machuca', 651646088, 'Granada', 'C/ Palencia', '3', 'nono.frailes@gmail.com'),
(5, 'Antonio', 'Machuca', 651646088, 'Granada', 'C/ Palencia', '3', 'nono.frailes@gmail.com'),
(8, 'Antonio', 'Machuca', 651646088, 'Granada', 'C/ Palencia', '1', 'nono.frailes@gmail.com'),
(10, 'Antonio', 'Machuca', 651646088, 'Granada', 'C/ Palencia', '1', 'nono.frailes@gmail.com'),
(11, 'Antonio', 'Machuca', 651646088, 'Granada', 'C/ Palencia', '1', 'nono.frailes@gmail.com');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `maillist`
--
ALTER TABLE `maillist`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `maillist`
--
ALTER TABLE `maillist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;