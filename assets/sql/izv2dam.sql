-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 14-10-2016 a las 02:00:00
-- Versión del servidor: 10.1.9-MariaDB
-- Versión de PHP: 7.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `izv2dam`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `maillist`
--

CREATE TABLE `maillist` (
  `id` int(11) NOT NULL,
  `nombre` varchar(20) DEFAULT NULL,
  `apellidos` varchar(40) DEFAULT NULL,
  `telefono` int(9) DEFAULT NULL,
  `ciudad` varchar(20) DEFAULT NULL,
  `calle` varchar(20) DEFAULT NULL,
  `numero` varchar(20) DEFAULT NULL,
  `email` varchar(320) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `maillist`
--

INSERT INTO `maillist` (`id`, `nombre`, `apellidos`, `telefono`, `ciudad`, `calle`, `numero`, `email`) VALUES
(1, 'Antonio', 'Machuca', 651646088, 'Granada', 'C/ Palencia', '3', 'nono.frailes@gmail.com'),
(3, 'Antonio', 'Machuca', 651646088, 'Granada', 'C/ Palencia', '3', 'nono.frailes@gmail.com'),
(5, 'Antonio', 'Machuca', 651646088, 'Granada', 'C/ Palencia', '3', 'nono.frailes@gmail.com'),
(8, 'Antonio', 'Machuca', 651646088, 'Granada', 'C/ Palencia', '1', 'nono.frailes@gmail.com'),
(10, 'Antonio', 'Machuca', 651646088, 'Granada', 'C/ Palencia', '1', 'nono.frailes@gmail.com'),
(11, 'Antonio', 'Machuca', 651646088, 'Granada', 'C/ Palencia', '1', 'nono.frailes@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mismatch_user`
--

CREATE TABLE `mismatch_user` (
  `user_id` int(6) NOT NULL,
  `join_date` datetime NOT NULL,
  `username` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `gender` varchar(1) NOT NULL,
  `birthdate` datetime NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  `state` varchar(2) DEFAULT NULL,
  `picture` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `mismatch_user`
--

INSERT INTO `mismatch_user` (`user_id`, `join_date`, `username`, `last_name`, `gender`, `birthdate`, `city`, `state`, `picture`, `password`) VALUES
(1, '2016-10-13 18:47:31', 'Antonio', 'Mudarra Machuca', 'M', '1996-03-29 00:00:00', 'Granada', 'Gr', 'img_Antonio.jpeg', '2a2ab400d355ac301859e4abb5432138'),
(2, '2016-10-13 21:03:54', 'Jesus', 'Valverde', 'M', '1999-12-25 00:00:00', 'Granada', 'Gr', 'img_Jesus.png', '2a2ab400d355ac301859e4abb5432138'),
(3, '2016-10-14 01:52:34', 'Maria', 'Lopez', 'F', '2000-01-13 00:00:00', 'Granada', 'Gr', 'img_Maria.jpg', '2a2ab400d355ac301859e4abb5432138');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `score_list`
--

CREATE TABLE `score_list` (
  `id` int(6) NOT NULL,
  `score` int(2) NOT NULL,
  `name` varchar(20) NOT NULL,
  `date` datetime DEFAULT NULL,
  `screenshot` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `maillist`
--
ALTER TABLE `maillist`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mismatch_user`
--
ALTER TABLE `mismatch_user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indices de la tabla `score_list`
--
ALTER TABLE `score_list`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `maillist`
--
ALTER TABLE `maillist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de la tabla `mismatch_user`
--
ALTER TABLE `mismatch_user`
  MODIFY `user_id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `score_list`
--
ALTER TABLE `score_list`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
