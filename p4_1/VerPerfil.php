<?php
require_once "_MySql.php";
require_once "_Nav.php";
if (!empty($_COOKIE["user_id"])) {
    //Has iniciado sesion
    ?>
    <div class="parallax1">
        <div class="caja">
            <h2>Bienvenido <?= $datos["username"] ?></h2>
            <img src="../assets/img/<?= $datos["picture"] ?>" alt="" class="img_usuario">
            <p><strong>Nombre: </strong><?= $datos["username"] ?></p>
            <p><strong>Apellidos: </strong><?= $datos["last_name"] ?></p>
            <p><strong>Ciudad: </strong><?= $datos["city"] ?></p>
            <p><strong>Fecha de nacimiento: </strong><?= $datos["birthdate"] ?></p>
            <?php

            ?>
        </div>
    </div>
    <div class="parallax2"></div>
    <div class="parallax3"></div>
    <?php
} else {
    header('Location: Index.php');
}
?>