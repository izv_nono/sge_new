<?php
/**
 * Created by PhpStorm.
 * User: dam
 * Date: 15/10/16
 * Time: 13:03
 */
require_once "_MySql.php";
require_once "_Nav.php";
require_once "_EditUser.php";


if (!empty($_COOKIE["user_id"])) {
    //Has iniciado sesion
    ?>
    <div class="parallax1">
        <form action="EditarPerfil.php" method="post" enctype="multipart/form-data">
            <table>
                <tr>
                    <td><label for="username"><b>Nombre: </b></label></td>
                    <td><input type="text" placeholder="Nombre" id="username" name="username" required></td>
                </tr>
                <tr>
                    <td><label for="nick_2"><b>Apellidos: </b></label></td>
                    <td><input type="text" placeholder="Apellidos" id="nick_2" name="last_name" required></td>
                </tr>
                <tr>
                    <td><label for="birthdate"><b>Fecha nacimiento: </b></label></td>
                    <td><input type="date" placeholder="" id="birthdate" name="birthdate" required></td>
                </tr>
                <tr>
                    <td><label for="state"><b>Provincia: </b></td>
                    <td><input type="text" placeholder="Provincia" id="state" name="state" required></td>
                </tr>
                <tr>
                    <td><label for="city"><b>Ciudad: </b></td>
                    <td><input type="text" placeholder="Ciudad" id="city" name="city" required></td>
                </tr>
                <tr>
                    <td><label for="gender_id"><b>Género: </b></label></td>
                    <td>
                        <select name="gender" id="gender_id" required>
                            <option value="M">Hombre</option>
                            <option value="F">Mujer</option>
                            <option value="O">Otro/a</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td><label for="picture"><b>Avatar: </b></td>
                    <td><input type="file" id="picture" name="picture" required></td>
                </tr>
                <tr>
                    <td>
                        <button class="red" type="reset">Cancelar</button>
                    </td>
                    <td>
                        <button class="green" type="submit">Registrarse</button>
                    </td>
                </tr>
                <tr>
                    <td><input type="hidden" name="formulario" value="signup"></td>
                    <td>
                        <p><?= (!empty($usuario_registrado) and $usuario_registrado == 1) ? "Usuario registrado" : "" ?></p>
                    </td>
                </tr>
            </table>
        </form>
    </div>
    <div class="parallax2"></div>
    <div class="parallax3"></div>
    <?php
} else {
    header('Location: Index.php');
}
?>