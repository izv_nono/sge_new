<?php
/**
 * Created by PhpStorm.
 * User: dam
 * Date: 23/9/16
 * Time: 14:15
 */
if (!empty($_POST["envio"])) {
    //Se ha enviado el mensaje
    if (!empty($_POST["email"])) {
        //Tengo email
        if (!empty($_POST["asunto"])) {
            //Tengo asunto
            if (!empty($_POST["mensaje"])) {
                //Tengo mensaje
                $mensaje = "Correcto";
            } else {
                $mensaje = "Falta el mensaje";
            }
        } else {
            $mensaje = "Falta el asunto";
        }
    } else {
        $mensaje = "Falta el email";
    }
} else {
    $mensaje = "";
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>SPAM</title>
    <style>
        body {
            background: url("elvis.jpeg");
        }

        form {
            margin-top: 20%;
            margin-left: 40%;
            margin-right: 40%;
            background: #FFF;
        }

        <?php
            if ($mensaje=="Correcto"){
                echo "
                .mensaje{
                    color: lime;
                }
                ";
            }else{
                echo "
                .mensaje{
                    color: red;
                }
                ";
            }
        ?>
    </style>
</head>
<body>
<h2>El Rey</h2>
<p class="mensaje"><?= $mensaje ?></p>
<form action="envio.php" method="post">
    <fieldset>
        <legend>Enviar mensaje</legend>
        <table>

            <tr>
                <td>
                    <label for="email">Email: </label>
                </td>
                <td>
                    <input type="email" name="email" id="email" maxlength="320">
                </td>
            </tr>
            <tr>
                <td>
                    <label for="asunto">Asunto: </label>
                </td>
                <td>
                    <input type="text" name="asunto" id="asunto" maxlength="50">
                </td>
            </tr>
            <tr>
                <td>
                    <label for="mensaje">Mensaje: </label>
                </td>
                <td>
                    <textarea name="mensaje" id="mensaje" cols="30" rows="10"></textarea>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="submit" value="Enviar" name="envio">
                </td>
            </tr>
        </table>
    </fieldset>


</form>
</body>
</html>
