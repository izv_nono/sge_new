<?php
/**
 * Created by PhpStorm.
 * User: dam
 * Date: 30/9/16
 * Time: 13:37
 */
if (!isset($_SERVER['PHP_AUTH_USER'])) {
    header('WWW-Authenticate: Basic realm="Mi dominio"');
    header('HTTP/1.0 401 Unauthorized');
    echo "Debes iniciar sesión para aceder";
    exit;
} elseif ($_SERVER["PHP_AUTH_USER"] == "admin" and $_SERVER["PHP_AUTH_PW"] == "admin") {

    require_once("mysql.php");

    ?>
    <!doctype html>
    <html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Admin</title>
    </head>
    <body>
    <!--    <pre>--><?//= print_r($_SERVER, true) ?><!--</pre>-->

    <?php

    $consulta = mysqli_query($con, "SELECT * FROM score_list");
    $datos = array();
    $datos2 = array();

    $i = 0;
    while ($row = mysqli_fetch_array($consulta, MYSQLI_ASSOC)) {
        foreach ($row as $key => $value) {
            $datos[$key] = $value;
        }
        $datos2[$i] = $datos;

        $i++;
    }
    ?>
    <table border="1">
        <thead>
        <tr>
            <th>ID</th>
            <th>Score</th>
            <th>Name</th>
            <th>Date</th>
            <th>Screenshot</th>
            <th>Borrar</th>
        </tr>
        </thead>

        <tbody>
        <?php
        foreach ($datos2 as $rows_id => $row) {
            ?>

            <tr>
                <form action="borrar.php" method="GET">
                    <?php
                    foreach ($row as $key => $value) {
                        ?>
                        <td>
                            <?php
                            if ($key != "screenshot") {
                                echo $value;
                            } else {
                                echo "<img src='../img/" . $value . "' alt='' height='100px'>";
                            }
                            ?>
                        </td>
                        <?php
                    }
                    ?>

                    <input type="hidden" name="id" value="<?= $row["id"] ?>">
                    <input type="hidden" name="img" value="<?= $row["screenshot"] ?>">

                    <td><input type="submit" value="Borrar"></td>

                </form>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
    </body>
    </html>
    <?php
}
?>