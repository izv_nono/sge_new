<?php
/**
 * Created by PhpStorm.
 * User: dam
 * Date: 30/9/16
 * Time: 13:52
 */
?>
<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Borrar</title>
</head>
<body>
<?php
require_once "mysql.php";

$id = !empty($_GET["id"]) ? $_GET["id"] : null;
$img = !empty($_GET["img"]) ? $_GET["img"] : null;
$confirmacion = !empty($_GET["confirmacion"]) ? $_GET["confirmacion"] : null;

if (($id != null and $img != null) and ($img != "no_pic.png" and $confirmacion == "si")) {
    //Borrar base de datos
    $consulta = mysqli_query($con, "DELETE FROM score_list WHERE id = " . $id . ";");
    //Borrar foto
    @unlink("img/" . $img);

} else {
    ?>
    <form action="borrar.php" method="get">
        <label for="confirmar">Confirmar</label><br>
        <input type="hidden" name="id" value="<?= $id ?>">
        <input type="hidden" name="img" value="<?= $img ?>">
        <input type="hidden" name="confirmacion" value="si">
        <input type="submit" value="Borrar"><br>
        <p><a href="admin.php">Volver</a></p>
    </form>
    <?php
}
mysqli_close($con);
?>
</body>
</html>
