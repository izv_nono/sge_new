<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<h2>Formulario</h2>

<form action="mostrar.php" method="post" enctype="multipart/form-data">
    <fieldset>
        <legend>Tu puntuacion en Guitar Wars!!</legend>
        <table border="1">
            <tr>
                <td><label for="name_id">Nombre</label></td>
                <td><input type="text" id="name_id" name="name"></td>
            </tr>
            <tr>
                <td><label for="score_id">Puntuación</label></td>
                <td><input type="number" id="score_id" name="score"></td>
            </tr>
            <tr>
                <td><label for="file_id">Archivo</label></td>
                <td><input type="file" id="file_id" name="screenshot" ></td>
            </tr>

            <tr>
                <td><input type="submit" value="Enviar" name="submit"></td>
            </tr>

        </table>
    </fieldset>
</form>


</body>
</html>
