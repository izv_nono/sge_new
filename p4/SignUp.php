<?php
/**
 * Created by PhpStorm.
 * User: dam
 * Date: 6/10/16
 * Time: 14:11
 */

$pass1 = md5(!empty($_POST["pass1"]) ? trim($_POST["pass1"]) : null);
$pass2 = md5(!empty($_POST["pass2"]) ? trim($_POST["pass2"]) : null);

$formulario = !empty($_POST["formulario"]) ? trim($_POST["formulario"]) : null;
$username = !empty($_POST["username"]) ? trim($_POST["username"]) : null;
$first_name = !empty($_POST["first_name"]) ? trim($_POST["first_name"]) : null;
$last_name = !empty($_POST["last_name"]) ? trim($_POST["last_name"]) : null;
$gender = !empty($_POST["gender"]) ? trim($_POST["gender"]) : null;
$birthdate = !empty($_POST["birthdate"]) ? trim($_POST["birthdate"]) : null;
$state = !empty($_POST["state"]) ? trim($_POST["state"]) : null;
$city = !empty($_POST["city"]) ? trim($_POST["city"]) : null;

if (!empty($_FILES["picture"])) {
    $picture_name = $_FILES["picture"]["name"];
    $picture_type = $_FILES["picture"]["type"];
    $picture_tmp_name = $_FILES["picture"]["tmp_name"];
    $picture_error = $_FILES["picture"]["error"];
    $picture_size = $_FILES["picture"]["size"];
} else {
    $picture_name = "";
    $picture_type = "";
    $picture_tmp_name = "";
    $picture_error = "";
    $picture_size = "";
}
if (!empty($first_name) and !empty($username) and !empty($pass1) and !empty($pass2) and $pass1 == $pass2 and
    !empty($last_name) and !empty($gender) and !empty($birthdate) and
    !empty($state) and !empty($city) and $picture_error == 0 and
    !empty($formulario) and $formulario == "signup"
) {
    $type_img = "";
    switch ($picture_type) {
        case "image/jpg":
            $type_img = "jpg";
            break;
        case "image/jpeg":
            $type_img = "jpeg";
            break;
        case "image/png":
            $type_img = "png";
            break;
        case "image/gif":
            $type_img = "gif";
            break;
    }
    $ruta_new_img = "img_" . $username . "." . $type_img;
    $img_user = move_uploaded_file($picture_tmp_name, RUTA_IMG . $ruta_new_img);

    $sql = "
    INSERT INTO mismatch_user 
                (user_id, join_date, username, first_name, last_name, gender, birthdate, city, state, picture, password) 
    VALUES      (NULL, '" . date("Y-m-d H:i:s") . "', '" . $username . "', '" ."', '" . $first_name . "', '"
        . $last_name . "', '" . $gender . "', '" . $birthdate . "', '" . $city . "', '" . $state . "', '"
        . $ruta_new_img . "', '" . $pass1 . "')";


//    echo "<p>" . $sql . "</p>";
    $usuario_registrado = 0;

    if (mysqli_query($con, $sql)) {
        $usuario_registrado = 1;
    } else {
        $usuario_registrado = 2;
    }

}


?>
<div class="parallax3" id="registrarse">
    <form action="Index.php" method="post" enctype="multipart/form-data">
        <table>
            <tr>
                <td><label for="username"><b>Nombre: </b></label></td>
                <td><input type="text" placeholder="Nombre" id="username" name="username" required></td>
            </tr>
            <tr>
                <td><label for="nick_2"><b>Apellidos: </b></label></td>
                <td><input type="text" placeholder="Apellidos" id="nick_2" name="last_name" required></td>
            </tr>
            <tr>
                <td><label for="birthdate"><b>Fecha nacimiento: </b></label></td>
                <td><input type="date" placeholder="" id="birthdate" name="birthdate" required></td>
            </tr>
            <tr>
                <td><label for="pass1"><b>Contraseña: </b></td>
                <td><input type="password" placeholder="Contraseña" id="pass1" name="pass1" required></td>
            </tr>
            <tr>
                <td><label for="pass2"><b>Repite la contraseña: </b></td>
                <td><input type="password" placeholder="Contraseña" id="pass2" name="pass2" required></td>
            </tr>

            <tr>
                <td><label for="state"><b>Provincia: </b></td>
                <td><input type="text" placeholder="Provincia" id="state" name="state" required></td>
            </tr>
            <tr>
                <td><label for="city"><b>Ciudad: </b></td>
                <td><input type="text" placeholder="Ciudad" id="city" name="city" required></td>
            </tr>
            <tr>
                <td><label for="gender_id"><b>Género: </b></label></td>
                <td>
                    <select name="gender" id="gender_id" required>
                        <option value="M">Hombre</option>
                        <option value="F">Mujer</option>
                        <option value="O">Otro/a</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td><label for="picture"><b>Avatar: </b></td>
                <td><input type="file" id="picture" name="picture" required></td>
            </tr>
            <tr>
                <td>
                    <button class="red" type="reset">Cancelar</button>
                </td>
                <td>
                    <button class="green" type="submit">Registrarse</button>
                </td>
            </tr>
            <tr>
                <td><input type="hidden" name="formulario" value="signup"></td>
                <td>
                    <p><?= (!empty($usuario_registrado) and $usuario_registrado == 1) ? "Usuario registrado" : "" ?></p>
                </td>
            </tr>
        </table>
    </form>
</div>
