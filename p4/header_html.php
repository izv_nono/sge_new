<?php
/**
 * Created by PhpStorm.
 * User: dam
 * Date: 11/10/16
 * Time: 12:41
 */
//print_r($_SERVER);
$dominio = "http://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mis Match</title>
    <link rel="stylesheet" href="../assets/css/style.css">
</head>
<body>

<nav>
    <ul>
        <?php
        switch ($_SERVER["REQUEST_URI"]) {
            case "/p4/Index.php":
                $ruta_index = true;
                break;
            case "/p4/VerPerfil.php":
                $ruta_usuario = true;
                break;

        }
        ?>
        <li class="<?= !empty($ruta_index) ? "active" : "" ?>"><a href="Index.php">Mis Match</a></li>
        <li class="<?= !empty($ruta_usuario) ? "active" : "" ?>"><a href="Usuario.php">Usuario</a></li>
        <li class="<?= !empty(0) ? "active" : "" ?>"><a href="#">Page 1</a></li>
        <li class="<?= !empty(0) ? "active" : "" ?>"><a href="#">Page 2</a></li>
        <li class="<?= !empty(0) ? "active" : "" ?>"><a href="#">Page 3</a></li>
    </ul>
</nav>
