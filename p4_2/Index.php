<?php
/**
 * Created by PhpStorm.
 * Date: 13/10/2016
 * Time: 22:05
 */
require_once "_MySql.php";
require_once "_Nav.php";

if (!empty($_SESSION["user_id"])) {
    //Has iniciado sesion
    ?>
    <div class="parallax1">
        <div class="caja">
            <table class="">
                <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Apellidos</th>
                    <th>Ciudad</th>
                    <th>Fecha de nacimiento</th>
                    <th>Foto</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $sql = "
                SELECT * 
                FROM mismatch_user 
                ";
                if ($result = mysqli_query($con, $sql)) {
                    $datos = array();
                    $datos2 = array();

                    $i = 0;
                    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
                        foreach ($row as $key => $value) {
                            $datos2[$key] = $value;
                        }
                        $datos[$i] = $datos2;


                        ?>
                        <tr>
                            <td><?= $datos[$i]["username"] ?></td>
                            <td><?= $datos[$i]["last_name"] ?></td>
                            <td><?= $datos[$i]["city"] ?></td>
                            <td><?= $datos[$i]["birthdate"] ?></td>
                            <td><img class="img_usuario2" src="../assets/img/<?= $datos[$i]["picture"] ?>" alt=""></td>
                        </tr>
                        <?php
                        $i++;
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="parallax2"></div>
    <div class="parallax3"></div>
    <?php
} else {
    //No has iniciado session
    require_once "_LogIn.php";
    require_once "index_user.php";
}
?>

