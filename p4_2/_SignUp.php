<?php
/**
 * Created by PhpStorm.
 * User: dam
 * Date: 6/10/16
 * Time: 14:11
 */
require_once "_MySql.php";

foreach ($_POST as $key_post => $value_post) {
    ${$key_post} = !empty($value_post) ? htmlentities(mysqli_real_escape_string($con, trim($value_post))) : null;
}

$pass1 = sha1(!empty($_POST["pass1"]) ? mysqli_real_escape_string($con, trim($_POST["pass1"])) : null);
$pass2 = sha1(!empty($_POST["pass2"]) ? mysqli_real_escape_string($con, trim($_POST["pass2"])) : null);
if (!empty($_FILES["picture"])) {
    $picture_name = $_FILES["picture"]["name"];
    $picture_type = $_FILES["picture"]["type"];
    $picture_tmp_name = $_FILES["picture"]["tmp_name"];
    $picture_error = $_FILES["picture"]["error"];
    $picture_size = $_FILES["picture"]["size"];
} else {
    $picture_name = "";
    $picture_type = "";
    $picture_tmp_name = "";
    $picture_error = "";
    $picture_size = "";
}


if (!empty($username) and !empty($first_name) and !empty($pass1) and !empty($pass2) and $pass1 == $pass2 and
    !empty($last_name) and !empty($gender) and !empty($birthdate) and
    !empty($state) and !empty($city) and $picture_error == 0 and
    !empty($formulario) and $formulario == "signup"
) {
    $type_img = "";
    switch ($picture_type) {
        case "image/jpg":
            $type_img = "jpg";
            break;
        case "image/jpeg":
            $type_img = "jpeg";
            break;
        case "image/png":
            $type_img = "png";
            break;
        case "image/gif":
            $type_img = "gif";
            break;
    }
    $ruta_new_img = "img_" . $username . "." . $type_img;
    $img_user = move_uploaded_file($picture_tmp_name, RUTA_IMG . $ruta_new_img);

    $sql = "
    INSERT INTO mismatch_user (user_id, join_date, username, first_name, last_name, gender, birthdate, city, state, picture, password) 
    VALUES (NULL, '" . date("Y-m-d H:i:s") . "', '" . $username . "', '"  . $first_name . "', '" . $last_name
        . "', '" . $gender . "', '" . $birthdate . "', '" . $city . "', '" . $state . "', '" . $ruta_new_img
        . "', '" . $pass1 . "')";


    echo "<p>" . $sql . "</p>";
    $usuario_registrado = 0;

    if (mysqli_query($con, $sql)) {
        $usuario_registrado = 1;
        header('Location: Index.php?u=new');
    } else {
        $usuario_registrado = 2;

    }


}



