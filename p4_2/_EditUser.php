<?php
/**
 * Created by PhpStorm.
 * User: dam
 * Date: 15/10/16
 * Time: 13:15
 */

$actualizado = false;
foreach ($_POST as $key_post => $value_post) {

    ${$key_post} = !empty($value_post) ? htmlentities(mysqli_real_escape_string($con, trim($value_post))) : null;

}
$pass1 = sha1(!empty($_POST["pass1"]) ? mysqli_real_escape_string($con, trim($_POST["pass1"])) : null);
$pass2 = sha1(!empty($_POST["pass2"]) ? mysqli_real_escape_string($con, trim($_POST["pass2"])) : null);
if (!empty($_SESSION["user_id"])) {

    if (!empty($username)) {
        $sql = "
        UPDATE mismatch_user 
        SET first_name = '" . $first_name . "', 
            last_name = '" . $last_name . "', 
            state = '" . $state . "', 
            city = '" . $city . "', 
            gender = '" . $gender . "'
        WHERE mismatch_user.user_id = " . $_SESSION["user_id"];

        if ($result = mysqli_query($con, $sql)) {
            $actualizado = true;

            /*
             * Para no causar confusion con los datos antiguos actualizamos la variable
             * de datos con los nuevos valores del usuario.
             */
            $sql = "
            SELECT * 
            FROM mismatch_user 
            WHERE user_id = '" . $_SESSION["user_id"] . "' 
            ";

            //    echo "<p>" . $sql . "</p>";

            if ($result = mysqli_query($con, $sql)) {
                $datos = array();
                $datos2 = array();

                $i = 0;
                while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
                    foreach ($row as $key => $value) {
                        $datos2[$key] = $value;
                    }
                    $datos = $datos2;

                    $i++;
                }
            }
        } else {
            $actualizado = false;
        }
    }


    if (!empty($edit_picture) and $edit_picture == "edit_picture" and !empty($_FILES["picture"])) {
        unlink($_SERVER["DOCUMENT_ROOT"] . "/assets/img/" . $datos["picture"]);

        $picture_name = $_FILES["picture"]["name"];
        $picture_type = $_FILES["picture"]["type"];
        $picture_tmp_name = $_FILES["picture"]["tmp_name"];
        $picture_error = $_FILES["picture"]["error"];
        $picture_size = $_FILES["picture"]["size"];

        switch ($picture_type) {
            case "image/jpg":
                $type_img = "jpg";
                break;
            case "image/jpeg":
                $type_img = "jpeg";
                break;
            case "image/png":
                $type_img = "png";
                break;
            case "image/gif":
                $type_img = "gif";
                break;
        }
        $ruta_new_img = "img_" . $datos["username"] . "." . $type_img;
        $img_user = move_uploaded_file($picture_tmp_name, RUTA_IMG . $ruta_new_img);


        $sql = "
        UPDATE mismatch_user 
        SET picture = '" . $ruta_new_img . "'
        WHERE mismatch_user.user_id = " . $_SESSION["user_id"];
        if ($result = mysqli_query($con, $sql)) {
            $actualizado = true;

        } else {

        }

    }

}

