<?php
/**
 * Created by PhpStorm.
 * Date: 13/10/2016
 * Time: 22:22
 */
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>p4</title>
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="pragma" content="no-cache" />

    <link href="../assets/css/style.css" rel="stylesheet">

</head>
<body>
<nav>
    <ul>
        <?php
        switch ($_SERVER["SCRIPT_NAME"]) {
            case "/p4_2/index.php":
            case "/p4_2/Index.php":
                $ruta_index = true;
                break;
            case "/p4_2/VerPerfil.php":
                $ruta_usuario = true;
                break;
            case "/p4_2/_LogOut.php":
                $ruta_log_out = true;
                break;
            case "/p4_2/EditarPerfil.php":
                $ruta_editar = true;
                break;
            case "/p4_2/Cuestionario.php":
                $ruta_cuestionario = true;
                break;
            case "/p4_2/Mismatch.php":
                $ruta_mismatch = true;
                break;
            default:
               // header("Location: index.php");

        }
        if (!empty($_SESSION["user_id"])) {

            $sql = "
            SELECT * 
            FROM mismatch_user 
            WHERE user_id = '" . $_SESSION["user_id"] . "' 
            ";

            //    echo "<p>" . $sql . "</p>";

            if ($result = mysqli_query($con, $sql)) {
                $datos = array();
                $datos2 = array();

                $i = 0;
                while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
                    foreach ($row as $key => $value) {
                        $datos2[$key] = $value;
                    }
                    $datos = $datos2;

                    $i++;
                }
            }
        }
        ?>

        <li class="<?= !empty($ruta_index) ? "active" : "" ?>"><a href="Index.php">Mis Match</a></li>
        <?php
        if (!empty($_SESSION["user_id"])) {
            ?>
            <li class="right <?= !empty($ruta_log_out) ? "active" : "" ?>"><a href="_LogOut.php">Cerrar sesión</a></li>
            <li class="right <?= !empty($ruta_usuario) ? "active" : "" ?>"><a href="VerPerfil.php">Ver perfil</a></li>
            <li class="right <?= !empty($ruta_editar) ? "active" : "" ?>"><a href="EditarPerfil.php">Editar perfil</a></li>
            <li class="right <?= !empty($ruta_cuestionario) ? "active" : "" ?>"><a href="Cuestionario.php">Cuestionario</a></li>
            <li class="right <?= !empty($ruta_mismatch) ? "active" : "" ?>"><a href="Mismatch.php">Mismatch</a></li>
            <li class="right nav_picture"><img src="../assets/img/<?= $_SESSION["picture"] ?>" alt=""></li>
            <?php
        } else {
            ?>

            <?php
        }
        ?>
    </ul>

</nav>
