<?php
/*
    SELECT topic.*
    FROM mismatch_user as user, mismatch_topic as topic, mismatch_category as category, mismatch_response as response
    WHERE user.user_id = response.user_id
    AND category.category_id = topic.category_id
    AND response.topic_id = topic.topic_id
    AND user.user_id = " . $_SESSION["user_id"] . "
 */


$sql_response = "
SELECT * 
FROM mismatch_response 
WHERE user_id = '" . $_SESSION['user_id'] . "'
";

$data_response = mysqli_query($con, $sql_response);
//Vacio
if (mysqli_num_rows($data_response) == 0) {

    $sql_topic = "
    SELECT topic_id 
    FROM mismatch_topic 
    ";
    $data_topic = mysqli_query($con, $sql_topic);
    $topics = array();
    while ($row = mysqli_fetch_array($data_topic)) {
        $topics[] = $row['topic_id'];
    }

    //Inicializamos a null
    foreach ($topics as $topic_id) {
        $insert_response = "
        INSERT INTO mismatch_response (user_id, topic_id) 
        VALUES ('" . $_SESSION['user_id'] . "', '" . $topic_id . "')";
        mysqli_query($con, $insert_response);
    }
}

//Actualizar
if (!empty($_POST['submit'])) {
    foreach ($_POST as $response_id => $response) {
        $update_response = "
        UPDATE mismatch_response 
        SET response = '" . $response . "' 
        WHERE response_id = '" . $response_id . "'";
        mysqli_query($con, $update_response);
    }
    $actuealizado = true;
}


//Sacamos todos los topics
$query_response_topic_user = "
    SELECT resp.*, topi.*, cate.*, 
    topi.name AS topic_name, 
    cate.name AS category_name
    FROM mismatch_response AS resp
    INNER JOIN mismatch_topic AS topi USING (topic_id) 
    INNER JOIN mismatch_category AS cate USING (category_id) 
    WHERE resp.user_id = '" . $_SESSION['user_id'] . "'";

$data_response_topic_user = mysqli_query($con, $query_response_topic_user);
$data_response_topic_user_array = array();

while ($row = mysqli_fetch_array($data_response_topic_user, MYSQLI_ASSOC)) {
    $data_response_topic_user_array[] = $row;
}

