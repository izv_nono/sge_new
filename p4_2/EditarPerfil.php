<?php
/**
 * Created by PhpStorm.
 * User: dam
 * Date: 15/10/16
 * Time: 13:03
 */
require_once "_MySql.php";
require_once "_Nav.php";
require_once "_EditUser.php";


if (!empty($_SESSION["user_id"])) {
    //Has iniciado sesion
    ?>
    <div class="parallax1">
        <form action="EditarPerfil.php" method="post" enctype="multipart/form-data">
            <table>
                <?php
                if ($actualizado == true) {
                    ?>
                    <tr>
                        <td></td>
                        <td class="success">
                            <p>Datos actualizados</p>
                        </td>
                    </tr>
                    <?php
                }
                ?>
                <tr>
                    <td><label for="first_name"><b>Nombre: </b></label></td>
                    <td><input type="text" placeholder="Nombre" id="username"
                               value="<?= !empty($datos["first_name"]) ? $datos["first_name"] : "" ?>" name="first_name"
                               required></td>
                </tr>
                <tr>
                    <td><label for="nick_2"><b>Apellidos: </b></label></td>
                    <td><input type="text" placeholder="Apellidos" id="nick_2" value="<?= $datos["last_name"] ?>"
                               name="last_name" required></td>
                </tr>
                <tr>
                    <td><label for="birthdate"><b>Fecha nacimiento: </b></label></td>
                    <td><input type="date" placeholder="" id="birthdate"
                               value="<?= date("Y-m-d", strtotime($datos["birthdate"])) ?>" name="birthdate" required>
                    </td>
                </tr>
                <tr>
                    <td><label for="state"><b>Provincia: </b></td>
                    <td><input type="text" placeholder="Provincia" id="state" value="<?= $datos["state"] ?>"
                               name="state" required></td>
                </tr>
                <tr>
                    <td><label for="city"><b>Ciudad: </b></td>
                    <td><input type="text" placeholder="Ciudad" id="city" value="<?= $datos["city"] ?>" name="city"
                               required></td>
                </tr>
                <tr>
                    <td><label for="gender_id"><b>Género: </b></label></td>
                    <td>
                        <select name="gender" id="gender_id" required>
                            <option <?= $datos["gender"] == "M" ? "selected" : "" ?> value="M">Hombre</option>
                            <option <?= $datos["gender"] == "F" ? "selected" : "" ?> value="F">Mujer</option>
                            <option <?= $datos["gender"] == "O" ? "selected" : "" ?> value="O">Otro/a</option>
                        </select>
                    </td>
                </tr><!--
                <tr>
                    <td><label for="picture"><b>Avatar: </b></td>
                    <td><input type="file" id="picture" name="picture"></td>
                </tr>-->
                <tr>
                    <td>
                        <button class="red" type="reset">Cancelar</button>
                    </td>
                    <td>
                        <button class="green" type="submit">Actualizar</button>
                    </td>
                </tr>
                <tr>
                    <td><input type="hidden" name="formulario" value="edit"></td>
                    <td>
                        <p>
                            <?= (!empty($usuario_registrado) and $usuario_registrado == 1) ? "Usuario registrado" : "" ?>
                        </p>
                    </td>
                </tr>
            </table>
        </form>
    </div>
    <div class="parallax2">

        <form action="EditarPerfil.php" method="post" enctype="multipart/form-data">
            <table>
                <tr>
                    <td></td>
                    <td><img class="img_usuario2" src="../assets/img/<?= $datos["picture"] ?>" alt=""></td>
                </tr>
                <tr>
                    <td><label for="id_picture">Editar Imagen</label></td>
                    <td><input type="file" name="picture" id="id_picture" required></td>
                </tr>
                <tr>
                    <td>
                        <input type="hidden" name="edit_picture" value="edit_picture">
                        <button class="red" type="reset">Cancelar</button>
                    </td>
                    <td>
                        <button class="green" type="submit">Registrarse</button>
                    </td>
                </tr>
            </table>
        </form>

    </div>
    <div class="parallax3"></div>
    <?php
} else {
    header('Location: EditarPerfil.php');
}
?>