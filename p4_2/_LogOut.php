<?php
/**
 * Created by PhpStorm.
 * User: Unchicodelavida
 * Date: 14/10/2016
 * Time: 0:19
 */
require_once "_MySql.php";

setcookie("username", "", -1, "/");
setcookie("user_id", "", -1, "/");
session_destroy();

header('Location: Index.php');