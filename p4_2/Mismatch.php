<?php
/**
 * Created by PhpStorm.
 * User: dam
 * Date: 28/10/16
 * Time: 9:29
 */

require_once "_MySql.php";
require_once "_Nav.php";
require_once "_Mismatch.php";
?>

<div class="parallax1">
    <div class="caja">
        <?php

        if (!empty($_SESSION["user_id"]) and $mismatch_user_id != -1) {
            $query = "
            SELECT *
            FROM mismatch_user 
            WHERE user_id = '" . $mismatch_user_id . "'";
            $data = mysqli_query($con, $query);
            if (mysqli_num_rows($data) == 1) {
                $fila = mysqli_fetch_array($data);
                echo "
                <table class='table_color'>
                    <tr>
                        <td>";
                if (!empty($fila['first_name']) && !empty($fila['last_name'])) {
                    echo "<h3>" . $fila['first_name'] . " " . $fila['last_name'] . "</h3>";
                }
                if (!empty($fila['city']) && !empty($fila['state'])) {
                    echo "<h4>" . $fila['city'] . " " . $fila['state'] . "</h4>";
                }
                echo "
                        </td>
                        <td>";
                if (!empty($fila['picture'])) {
                    echo "<img width='100px' src='../assets/img/" . $fila['picture'] . "'>";
                }
                echo "
                        </td>
                    </tr>
                </table>";

                foreach ($mismatch_topics as $topic) {
                    echo "<p>" . $topic . "</p>";
                }


            }
        } ?>
    </div>

</div>


